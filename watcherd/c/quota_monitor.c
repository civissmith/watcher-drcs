/*******************************************************************************
* @Title: quota_monitor.c
*
* @Author: Phil Smith
*
* @Date: Fri, 23-Dec-16 01:54PM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Implement the logic for the quota monitor thread. This thread will
*           ensure that only (b) backups are captured. (b == QUOTA number from
*           config file).
*
*
*******************************************************************************/
#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>
#include "common.h"
#include "config.h"
#include "server_functions.h"
#include "watcherd_global.h"

bool keep_running = true;

/**
 * archive_node_t:
 * Type to encapsulate archives within the archive list
 **/
struct archive_node_t {

    char name[BUFSIZE];
    long mod_time;
    struct archive_node_t *next;
};

/**
 * archive_list_t:
 * List type to contain archives in the archive_node_t containers
 **/
struct archive_list_t {
    struct archive_node_t *head;
    struct archive_node_t *cursor;
    int size;
};

static void __reset_archive_list( struct archive_list_t *archives );
static void __destroy_archive_list( struct archive_list_t *archives );


/***
 * Function: void* quota_monitor()
 * Inputs: void* args (cast to struct config_t*) - configuration information
 *         from the config file
 * Outputs: NONE
 * Returns: NONE
 * Description: This function runs as separate thread. It will monitor the
 * backing store to determine if it has gone over the quota. Once the quota is
 * exceeded, the oldest archive is removed.
 ***/
void *quota_monitor( void *args )
{

    struct stat statbuf;
    struct config_t *config = (struct config_t*)args;

    /* It is assumed that the master has already verified that the backing store
     * points to a valid directory */
    DIR *dir_ptr = NULL;
    struct dirent *dir_entry = NULL;
    int index = 0;

    /* If the quota is disabled, return */
    if ( config->quota == QUOTA_DISABLED ) {
        syslog( LOG_ERR, "Quota monitor is quitting\n" );
        closedir( dir_ptr );
        return NULL;
    }
    struct archive_list_t *archive_list = NULL;

    /* For now, assume some event has occurred that triggers quota monitor */
    while ( keep_running ) {

        pthread_mutex_lock( &quota_mutex );
        while ( check_quota == false ) {
            pthread_cond_wait( &quota_cv, &quota_mutex );
        }
        //// BEGIN CRITICAL SECTION ////

        if ( !test_mode_enabled ){
            dir_ptr = opendir( config->backing_store );
        }else{
            mkdir( config->test_backing_store, 0770 );
            dir_ptr = opendir( config->test_backing_store );
            if ( dir_ptr == NULL ){
                /* Don't take the system out of test mode. This would require
                 * lock/unlock ordering (not worth it). The test harness will
                 * consider this a test a failure */
                pthread_mutex_unlock( &quota_mutex );
                continue;
            }
        }
        archive_list = malloc( sizeof( struct archive_list_t ) );
        /* Create the list to hold the archive data */
        if ( archive_list == NULL ) {
            fprintf( stderr, "Quota failed to get memory for archive list\n" );
            closedir( dir_ptr );
            return NULL;
        }
        memset( archive_list, '\0', sizeof( struct archive_list_t ) );
        archive_list->size = 0;


        /* Read the directory contents and count the number of archives */
        dir_entry = readdir( dir_ptr );
        while ( dir_entry != NULL ) {

            /* Skip the parent directory and the current directory */
            if ( ( strncmp( dir_entry->d_name, ".", strlen( dir_entry->d_name ) ) == 0 ) ||
                 ( strncmp( dir_entry->d_name, "..", strlen( dir_entry->d_name ) ) == 0 ) ) {

                dir_entry = readdir( dir_ptr );
                continue;
            }

            /* Count only the archive files */
            if ( strstr( dir_entry->d_name, "watcher_archive" ) != NULL ) {

                /* Create an entry in the archive list */
                struct archive_node_t *archive = NULL;
                archive = malloc( sizeof( struct archive_node_t ) );
                if ( archive == NULL ) {
                    fprintf( stderr,
                             "Quota manager could not allocate archive nodes\n" );
                    closedir( dir_ptr );
                    __destroy_archive_list( archive_list );
                    pthread_mutex_unlock( &quota_mutex );
                    return NULL;
                }
                memset( archive, '\0', sizeof( struct archive_node_t ) );
                strncpy( archive->name, dir_entry->d_name,
                         sizeof( archive->name ) );
                archive->next = NULL;


                /* Get the file modification data */
                stat( archive->name, &statbuf );
                archive->mod_time = statbuf.st_mtim.tv_sec;

                /* Insert the archives in sorted order */
                if ( archive_list->head == NULL ) {

                    /* The first element is always the head */
                    archive_list->head = archive;
                    archive_list->cursor = archive_list->head;
                    archive_list->size = 1;
                } else {

                    __reset_archive_list( archive_list );
                    if ( archive->mod_time > archive_list->head->mod_time ) {
                        /* The archive is newer than the current head, so take
                         * the head position */
                        archive->next = archive_list->head;
                        archive_list->head = archive;
                    } else {
                        /* The archive's position must be found */
                        while ( archive_list->cursor->next != NULL &&
                                archive->mod_time <
                                archive_list->cursor->next->mod_time ) {
                            archive_list->cursor = archive_list->cursor->next;
                        }

                        /* The cursor has landed at the end, so append */
                        if ( archive_list->cursor->next == NULL ) {
                            archive_list->cursor->next = archive;
                        } else {
                            /* Else insert the new archive in the middle of the list */
                            archive->next = archive_list->cursor->next;
                            archive_list->cursor->next = archive;
                        }
                    }
                    archive_list->size += 1;
                }
            }
            dir_entry = readdir( dir_ptr );
        }

        /* All archives are now in the list. If they're under quota, everything
         * is fine. */
        if ( archive_list->size > config->quota ) {
            __reset_archive_list( archive_list );
            /* Fast-forward to the nodes that need to be deleted */
            for ( index = 0; index < config->quota; index++ ) {
                archive_list->cursor = archive_list->cursor->next;
            }
            /* Now delete each file that is over the quota */
            while ( archive_list->cursor != NULL ) {
                unlink( archive_list->cursor->name );
                memset( archive_list->cursor->name, '\0',
                        sizeof( archive_list->cursor->name ) );
                archive_list->cursor = archive_list->cursor->next;
                archive_list->size -= 1;
            }
        }
        __destroy_archive_list( archive_list );
        closedir( dir_ptr );

        check_quota = false;
        //// END CRITICAL SECTION ////
        pthread_mutex_unlock( &quota_mutex );
    }
    return NULL;
}

/***
 * Function: void __destroy_archive_list(struct archive_list_t* archives)
 * Inputs: struct archive_list_t* archives - list of archives
 * Outputs: NONE
 * Returns: NONE
 * Description: Frees the resources associated with the given archive list
 ***/
static void __destroy_archive_list( struct archive_list_t *archives )
{

    if ( archives->size == 0 ) {
        return;
    }

    __reset_archive_list( archives );
    struct archive_node_t *delete;
    while ( archives->cursor != NULL ) {
        delete = archives->cursor;
        archives->cursor = archives->cursor->next;
        free( delete );
    }
    archives->head = NULL;
    free( archives );
}

/***
 * Function: void __reset_archive_list(struct archive_list_t* archives)
 * Inputs: struct archive_list_t* archives - archive list to reset
 * Outputs: NONE
 * Returns: NONE
 * Description: Reset the given archive list cursor to the head
 ***/
static void __reset_archive_list( struct archive_list_t *archives )
{
    archives->cursor = archives->head;
}
