/*******************************************************************************
* @Title: watcher_global.c
*
* @Author: Phil Smith
*
* @Date: Thu, 16-Mar-17 10:58AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Instantiation for the global variables.
*
*
*******************************************************************************/
#include <pthread.h>
#include <stdbool.h>
#include "watcherd_global.h"

/* Condition variable to kick off the quota monitor */
pthread_cond_t quota_cv = PTHREAD_COND_INITIALIZER;
/* Mutex to synchronize the server with the quota monitor */
pthread_mutex_t quota_mutex = PTHREAD_MUTEX_INITIALIZER;
/* Command variable to run the quota monitor */
bool check_quota = false;

/* Mutex to synchronize the auto-test mode */
pthread_mutex_t test_mode_mutex = PTHREAD_MUTEX_INITIALIZER;
bool test_mode_enabled = false;
