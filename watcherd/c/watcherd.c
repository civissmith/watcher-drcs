/*******************************************************************************
* @Title: watcherd.c
*
* @Author: Phil Smith
*
* @Date: Wed, 14-Dec-16 01:36PM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: The main implementation of the watcher server
*
*******************************************************************************/
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/file.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>
#include "common.h"
#include "config.h"
#include "server.h"
#include "server_functions.h"

#define USAGE                                                                 \
"usage:\n"                                                                    \
"  watcherd       [options]\n"                                                \
"options:\n"                                                                  \
"  -c         Configuration file (Default: /etc/sysconfig/watcherd-config)\n" \
"  -n         Do not become a daemon\n"                                       \
"  -h         Show this help message\n"

extern bool keep_running;

/***
 * Function: catch_shutdown(int signo, siginfo_t* sig, void* unused)
 * Description: Catches the shutdown event.
 ***/
void catch_shutdown( int signo, siginfo_t * sig, void *unused )
{
    keep_running = false;
}

/***
 * Function: __stop_commanded()
 * Description: Returns true if the program is commanded to stop.
 ***/
bool __stop_commanded();

/***
 * Function: __daemonize()
 * Description: Causes the program to drop all controlling terminals and become
 *              a daemon.
 ***/
void __daemonize(  );

/***
 * Function: __is_valid(const char* path)
 * Description: Checks to determine if a given path is valid or not.
 ***/
bool __is_valid( const char *path );


/***
 * Function: write_pid_file()
 * Description: Writes the pid file to allow file-based locking to work.
 ***/
static void write_pid_file(  )
{
    int pid_file = open( "/var/run/watcherd.pid", O_CREAT | O_RDWR, 0666 );
    int rc = flock( pid_file, LOCK_EX | LOCK_NB );
    if ( rc ) {
        if ( EWOULDBLOCK == errno ) {
            exit( EXIT_SUCCESS );   // another instance is running
        }
    }

    pid_t pid = getpid(  );
    char pid_string[NAME_LEN] = { '\0' };
    sprintf( pid_string, "%d\n", pid );
    write( pid_file, pid_string, strlen( pid_string ) );
}


/***
 * Function: main (int args, char** argv)
 * Description: Main entry for the watcherd server.
 ***/
int main( int argc, char **argv )
{

    int option_char;
    char *config_name = NULL;
    bool spawn_daemon = true;

    struct sigaction sig;
    sig.sa_flags = SA_SIGINFO;
    sigemptyset( &sig.sa_mask );
    sig.sa_sigaction = catch_shutdown;
    sigaction( SIGINT, &sig, NULL );

    if ( __stop_commanded() == true ){
        exit(0);
    }

    // Parse and set command line arguments
    while ( ( option_char = getopt( argc, argv, "c:nh" ) ) != -1 ) {
        switch ( option_char ) {
        case 'c':
            config_name = optarg;
            break;
        case 'n':
            spawn_daemon = false;
            break;
        case 'h':
            fprintf( stdout, "%s", USAGE );
            exit( EXIT_SUCCESS );
            break;
        default:
            fprintf( stderr, "%s", USAGE );
            exit( EXIT_FAILURE );
        }
    }

    keep_running = true;
    if ( config_name == NULL ) {
        config_name = "/etc/sysconfig/watcherd-config";
    }

    struct config_t* config= NULL;
    config = malloc(sizeof(struct config_t));
    parse_config( config_name, config );

    if ( !__is_valid( config->backing_store ) ) {
        fprintf( stderr, "FATAL: %s is not a valid directory\n",
                 config->backing_store );
        fprintf( stderr, "       Make sure:\n" );
        fprintf( stderr, "         1. BACKING_STORE points to a DIRECTORY\n" );
        fprintf( stderr, "         2. User has W/X permissions\n" );
        exit( EXIT_FAILURE );
    }
    if ( chdir( config->backing_store ) < 0 ) {
        fprintf( stderr, "Could not change to backing store\n" );
    }

    if ( spawn_daemon ){
        __daemonize(  );
    }
    write_pid_file(  );

    pthread_t quota_thread;

    pthread_create( &quota_thread, NULL, &quota_monitor, config );
    syslog( LOG_INFO, "Watcher DRCS Server Started" );
    syslog( LOG_INFO, "Using configuration: %s", config_name );
    syslog( LOG_INFO, "Backing Store: %s", config->backing_store );
    syslog( LOG_INFO, "Serving on port: %hu", config->port );

    Server *server = wa_get_server(  );
    wa_set_server_port( server, config->port );
    wa_set_server_args( server, config );
    wa_register_server( server, &handle_connection );
    wa_serve( server );

    free(config);
    wa_destroy_server( server );

    /* Silence compiler (-Wreturn-type) */
    return ( 0 );
}


bool __is_valid( const char *path )
{

    /* Invariant: A null path is automatically invalid */
    if ( path == NULL ) {
        return false;
    }
    struct stat statbuf;
    stat( path, &statbuf );

    /* The path does not point to a directory */
    if ( !S_ISDIR( statbuf.st_mode ) ) {
        return false;
    }

    /* The user does not have permissions */
    if ( !( statbuf.st_mode & ( S_IWUSR | S_IXUSR ) ) ) {
        return false;
    }
    return true;
}


void __daemonize(  )
{

    int i;
    int fd0;
    int fd1;
    int fd2;

    pid_t pid;
    struct rlimit rl;
    struct sigaction sa;

    /* Clear the creation mask */
    umask( 0 );

    /* Get the max number of file descriptors */
    if ( getrlimit( RLIMIT_NOFILE, &rl ) < 0 ) {
        fprintf( stderr, "Could not daemonize the server\n" );
        exit( EXIT_FAILURE );
    }


    /* Become a session leader to lose the CTTY */
    if ( ( pid = fork(  ) ) < 0 ) {
        fprintf( stderr, "Could not fork (1)\n" );
        exit( EXIT_FAILURE );
    } else if ( pid != 0 ) {
        exit( EXIT_SUCCESS );
    }

    setsid(  );


    /* Ensure future opens won't allocate CTTYs */
    sa.sa_handler = SIG_IGN;
    sigemptyset( &sa.sa_mask );
    sa.sa_flags = 0;
    if ( sigaction( SIGHUP, &sa, NULL ) < 0 ) {
        fprintf( stderr, "Could not ignore SIGHUP\n" );
        exit( EXIT_FAILURE );
    }
    if ( ( pid = fork(  ) ) < 0 ) {
        fprintf( stderr, "Could not ignore fork (2)\n" );
        exit( EXIT_FAILURE );
    } else if ( pid != 0 ) {
        exit( EXIT_SUCCESS );
    }

    if ( rl.rlim_max == RLIM_INFINITY ) {
        rl.rlim_max = 1024;
    }

    for ( i = 0; i < rl.rlim_max; i++ ) {
        close( i );
    }


    /* Associate stdin, stdout and stderr to /dev/null */
    fd0 = open( "/dev/null", O_RDWR );
    fd1 = dup( 0 );
    fd2 = dup( 0 );

    /* Open the log file */
    if ( fd0 != 0 || fd1 != 1 || fd2 != 2 ) {
        syslog( LOG_ERR, "Unexpected file descriptors %d %d %d", fd0, fd1,
                fd2 );
        exit( EXIT_FAILURE );
    }
}


bool __stop_commanded(){

    FILE* file = fopen(STOP_FILE, "r");

    if( file == NULL ){
        return false;
    }

    fclose(file);
    return true;
}
