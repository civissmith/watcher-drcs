/*******************************************************************************
* @Title: handle_connections.c
*
* @Author: Phil Smith
*
* @Date: Wed, 01-Mar-17 05:40AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Module to contain the handler function for the watcherd connections.
*
*******************************************************************************/
/* Include additional feature test macros for older GLIBC versions */
#if __GNUC__ < 5
#define _XOPEN_SOURCE 500
#include <strings.h>
#endif
#include <arpa/inet.h>
#include <dirent.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>
#include "common.h"
#include "config.h"
#include "file.h"
#include "protocol.h"
#include "watcherd_global.h"

#define ERR_404 "404 - Not Found\n"
#define ERR_403 "403 - Forbidden\n"

/***
 * Enumerate the command codes.
 ***/
enum COMMAND_CODES { CMD_NONE, CMD_SEND, CMD_LIST, CMD_RESTORE, CMD_STATS,
                     CMD_TEST_START, CMD_TEST_STOP };
/***
 * struct header_t
 * Encapsulate information about a received connection header.
 ***/
struct header_t {
    int command;
    char file_name[TINYBUF];
    int file_size;
};


/***
 * Function: find_header()
 * Description: Finds the header in a given transfer buffer.
 ***/
off_t find_header( char *buffer, ssize_t * buffer_size,
                   struct header_t *header );

/***
 * Function: handle_connection(void* args)
 * Description: Handle connections in to the server.
 ***/
void *handle_connection( void *args )
{
    struct config_t* config = (struct config_t*) args;
    ssize_t rx_size = 1;
    char rx_buffer[BUFSIZE];
    char tx_buffer[BUFSIZE];
    off_t header_end = 0;
    int output;
    struct header_t header_info;
    int bytes_rx = 0;
    int bytes_tx = 0;
    off_t eof = 0;
    off_t file_pos = 0;
    ssize_t actual = 0;
    ssize_t commanded = 0;
    char cwd_name[PATH_MAX] = { '\0' };
    DIR *dir_ptr = NULL;
    struct dirent *dir_entry = NULL;
    char file_name[NAME_MAX] = { '\0' };


    //FIRST: Parse the header to determine the command
    if ( header_end == 0 ) {
        while ( rx_size > 0 ) {
            memset( rx_buffer, '\0', BUFSIZE );

            /* Read the data from the client */
            rx_size = recv( config->socket_fd, rx_buffer, BUFSIZE, 0 );
            if ( rx_size < 0 ) {
                syslog( LOG_ERR, "Bad read on client socket" );
            }
            header_end = find_header( rx_buffer, &rx_size, &header_info );

            /* On the first read, either a header is found or the connection
             * is discarded */
            if ( header_end != 0 ) {
                break;
            } else {
                shutdown( config->socket_fd, SHUT_RDWR );
                close( config->socket_fd );
                return NULL;
            }
        }
    }

    /* At this point, a valid protocol header was found. The work to be done
     * will depend on the command */
    switch ( header_info.command ) {

    case CMD_SEND:

        /* If the test mode is enabled, the file must be written to the test dir
         * otherwise it must be in the main dir. */
        if( test_mode_enabled ){
            mkdir(config->test_backing_store, 0770);
            chdir(config->test_backing_store);
        }else{
            chdir(config->backing_store);
        }

        /* Make sure there is no attempt to write outside of the working directory */
        if( ( header_info.file_name[0] == '.' && header_info.file_name[1] == '.' ) ||
            ( header_info.file_name[0] == '/' ) ){
            syslog( LOG_ERR, "Attempted Illegal overwrite %s", header_info.file_name );
            break;
        }

        output =
            open( header_info.file_name, O_WRONLY | O_TRUNC | O_CREAT, 0775 );
        if ( output < 0 ) {
            syslog( LOG_ERR, "Could not open file" );
        }

        /* The first packet will have header information that will need to
         * be stripped. */

        pwrite( output, rx_buffer + header_end, rx_size, 0 );
        bytes_rx = rx_size;

        while ( bytes_rx < header_info.file_size ) {
            memset( rx_buffer, '\0', BUFSIZE );
            /* Read the data from the client */
            rx_size = recv( config->socket_fd, rx_buffer, BUFSIZE, 0 );
            pwrite( output, rx_buffer, rx_size, bytes_rx );
            if ( rx_size > 0 ) {
                bytes_rx += rx_size;
            } else {
                syslog( LOG_ERR, "Error encountered writing file %s",
                        header_info.file_name );
                break;
            }
        }
        close( output );
        break;

    case CMD_LIST:

        syslog( LOG_INFO, "Server rx'ed a LIST command" );

        memset( cwd_name, '\0', PATH_MAX );
        /* The current directory should either be the BACKING_STORE or the
         * TEST_BACKING_STORE depending on if test_mode_enabled is set */
        getcwd( cwd_name, PATH_MAX );

        dir_ptr = opendir( cwd_name );

        /* Read the contents of the backing store and send the list */
        dir_entry = readdir( dir_ptr );

        while ( dir_entry != NULL ) {

            /* Skip the parent directory and current directory */
            if ( ( strncmp( dir_entry->d_name, ".", strlen( dir_entry->d_name ) ) == 0 ) ||
                 ( strncmp( dir_entry->d_name, "..", strlen( dir_entry->d_name ) ) == 0 ) ) {
                dir_entry = readdir( dir_ptr );
                continue;
            }
            memset( file_name, '\0', NAME_MAX );
            snprintf( file_name, NAME_MAX, "%s\n", dir_entry->d_name );
            commanded = strlen( file_name );

            actual = send( config->socket_fd, file_name, commanded, 0 );
            while ( actual != commanded ) {
                actual =
                    send( config->socket_fd, ( void * )( file_name + actual ), commanded,
                          0 );
                if ( actual < 0 ) {
                    syslog( LOG_ERR, "Could not transfer file name: %s",
                            dir_entry->d_name );
                    continue;
                }
            }
            dir_entry = readdir( dir_ptr );
        }
        closedir( dir_ptr );
        break;

    case CMD_RESTORE:
        syslog( LOG_INFO, "Server rx'ed a RESTORE command" );

        if ( ( strncmp( header_info.file_name, ".", strlen( header_info.file_name ) ) == 0 ) ||
             ( strncmp( header_info.file_name, "..", strlen( header_info.file_name ) ) == 0 ) ) {
            send( config->socket_fd, ERR_404, strlen( ERR_404 ), 0 );
            break;
        }

        /* For minor security, don't honor requests with absolute or relative paths */
        if ( ( header_info.file_name[0] == '/'                                      ||
              ( header_info.file_name[0] == '.' && header_info.file_name[1] == '/') ||
              ( header_info.file_name[0] == '.' && header_info.file_name[1] == '.')    )){
            send( config->socket_fd, ERR_403, strlen( ERR_403 ), 0 );
            syslog( LOG_ERR, "Attempted Illegal restoration %s", header_info.file_name );
            break;
        }

        output = open( header_info.file_name, O_RDONLY, 0775 );

        /* The file couldn't be opened. Return a "File Not Found" error */
        if ( output < 0 ) {
            send( config->socket_fd, ERR_404, strlen( ERR_404 ), 0 );
            break;
        }

        /* Get the EOF location so we know when to stop reading */
        eof = lseek( output, 0, SEEK_END );
        file_pos = 0;

        /* Stream file contents until EOF is reached */
        while ( file_pos < eof ) {
            bytes_tx = pread( output, tx_buffer, BUFSIZE, file_pos );
            commanded = bytes_tx;
            actual = send( config->socket_fd, tx_buffer, commanded, 0 );
            while ( actual != commanded ) {
                actual =
                    send( config->socket_fd, ( void * )( tx_buffer + actual ), commanded,
                          0 );
                if ( actual < 0 ) {
                    syslog( LOG_ERR, "Could not transfer file name: %s",
                            dir_entry->d_name );
                    continue;
                }
            }
            file_pos += bytes_tx;
        }

        close( output );
        break;

    /* Return the configuration stats */
    case CMD_STATS:

        syslog( LOG_INFO, "Server rx'ed a STATS command" );

        /* Send the QUOTA size */
        memset( tx_buffer, '\0', sizeof(tx_buffer) );
        snprintf( tx_buffer, sizeof(tx_buffer), "QUOTA: %d\n", config->quota );
        commanded = strlen( tx_buffer );

        actual = send( config->socket_fd, tx_buffer, commanded, 0 );
        while ( actual != commanded ) {
            actual =
                send( config->socket_fd, ( void * )( tx_buffer + actual ), commanded,
                      0 );
            if ( actual < 0 ) {
                syslog( LOG_ERR, "Could not transfer file name: %s",
                        dir_entry->d_name );
                continue;
            }
        }

        snprintf( tx_buffer, sizeof(tx_buffer), "BACKING_STORE: %s\n", config->backing_store );
        commanded = strlen( tx_buffer );

        actual = send( config->socket_fd, tx_buffer, commanded, 0 );
        while ( actual != commanded ) {
            actual =
                send( config->socket_fd, ( void * )( tx_buffer + actual ), commanded,
                      0 );
            if ( actual < 0 ) {
                syslog( LOG_ERR, "Could not transfer file name: %s",
                        dir_entry->d_name );
                continue;
            }
        }
        snprintf( tx_buffer, sizeof(tx_buffer), "TEST_BACKING_STORE: %s\n", config->test_backing_store );
        commanded = strlen( tx_buffer );

        actual = send( config->socket_fd, tx_buffer, commanded, 0 );
        while ( actual != commanded ) {
            actual =
                send( config->socket_fd, ( void * )( tx_buffer + actual ), commanded,
                      0 );
            if ( actual < 0 ) {
                syslog( LOG_ERR, "Could not transfer file name: %s",
                        dir_entry->d_name );
                continue;
            }
        }


        break;

    /* Enable the Automated tester */
    case CMD_TEST_START:
        syslog( LOG_DEBUG, "Starting Test Mode");
        pthread_mutex_lock( &test_mode_mutex );
        test_mode_enabled = true;
        pthread_mutex_unlock( &test_mode_mutex );

        chdir( config->test_backing_store );
        break;

    /* Disable the Automated tester */
    case CMD_TEST_STOP:
        syslog( LOG_DEBUG, "Stopping Test Mode");
        pthread_mutex_lock( &test_mode_mutex );
        test_mode_enabled = false;
        pthread_mutex_unlock( &test_mode_mutex );

        chdir( config->backing_store );
        file_remove_dir(config->test_backing_store);
        break;

    default:
        syslog( LOG_ERR, "Watcher server received invalid header\n" );
        break;
    }
    close( config->socket_fd );

    /* Tell the quota monitor it's safe to run */
    pthread_mutex_lock( &quota_mutex );
    check_quota = true;
    pthread_mutex_unlock( &quota_mutex );
    pthread_cond_signal( &quota_cv );
    return NULL;
}


off_t find_header( char *buffer, ssize_t * buffer_size,
                   struct header_t * header )
{

    char *save_ptr;
    char *protocol = NULL;
    char *command = NULL;
    char *file_name = NULL;
    char *file_size = NULL;
    char *term_offset = NULL;
    char *header_string = NULL;
    off_t start_offset = 0;

    /* Ensure that the returned header is sane */
    header->command = CMD_NONE;
    memset( header->file_name, '\0', TINYBUF );
    header->file_size = 0;

    header_string = buffer;

    /* First check to make sure a valid terminator is in the header */
    term_offset = strstr( buffer, "\r\n\r\n" );
    if ( term_offset == NULL ) {
        /* There is no header in this packet */
        return 0;
    }

    start_offset = ( term_offset + 4 ) - buffer;
    /* Adjust the buffer size to remove the length of the terminator */
    *buffer_size -= ( ( term_offset + 4 ) - buffer );
    /* ?? Point the head of the buffer at the end of the terminator - is... is this legal?? */
    buffer = term_offset + 4;

    protocol = strtok_r( header_string, " \n\r", &save_ptr );
    if ( protocol == NULL ) {
        syslog( LOG_ERR, "Invalid protocol detected" );
        syslog( LOG_ERR, "[[\n%s]]\n", header_string );
        return 0;
    }
    /* Return false if a valid protocol header is not found */
    if ( strncmp( protocol, W_PROTOCOL, strlen( W_PROTOCOL ) ) != 0 ) {
        return 0;
    }

    /* The command parameter will determine what data gets saved next */
    command = strtok_r( NULL, " \n\r", &save_ptr );

    /* A Send command was detected */
    if ( strncasecmp( command, "SEND", strlen( "SEND" ) ) == 0 ) {
        header->command = CMD_SEND;

        /* Get the name of the file to write */
        file_name = strtok_r( NULL, " \n\r", &save_ptr );
        if ( file_name == NULL ) {
            /* Treat a malformed header like no header */
            return 0;
        }

        /* Disrupt an attempt to leave the working directory */
        sprintf( header->file_name, "%s", file_name );
        file_size = strtok_r( NULL, " \n\r", &save_ptr );
        if ( file_size == NULL ) {
            /* Treat a malformed header like no header */
            return 0;
        }
        header->file_size = atoi( file_size );
    }
    /* A List command was detected */
    if ( strncasecmp( command, "LIST", strlen( "LIST" ) ) == 0 ) {
        header->command = CMD_LIST;
    }
    /* A Restore command was detected */
    if ( strncasecmp( command, "RESTORE", strlen( "RESTORE" ) ) == 0 ) {
        header->command = CMD_RESTORE;

        /* Get the name of the file to read */
        file_name = strtok_r( NULL, " \n\r", &save_ptr );
        if ( file_name == NULL ) {
            /* Treat a malformed header like no header */
            return 0;
        }
        sprintf( header->file_name, "%s", file_name );
    }
    if ( strncasecmp( command, "STATS", strlen( "STATS" ) ) == 0 ) {
        header->command = CMD_STATS;
    }
    /* An auto-test start command was received */
    if ( strncasecmp( command, "TEST_START", strlen( "TEST_START" ) ) == 0 ) {
        header->command = CMD_TEST_START;
    }
    /* An auto-test start command was received */
    if ( strncasecmp( command, "TEST_STOP", strlen( "TEST_STOP" ) ) == 0 ) {
        header->command = CMD_TEST_STOP;
    }

    return start_offset;
}
