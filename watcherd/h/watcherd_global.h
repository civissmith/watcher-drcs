/*******************************************************************************
* @Title: watcherd_global.h
*
* @Author: Phil Smith
*
* @Date: Thu, 16-Mar-17 11:04AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This header file contains global instance references.
*
*******************************************************************************/
#ifndef __WATCHER_GLOBAL__
#define __WATCHER_GLOBAL__

/* Condition variable to kick off the quota monitor */
extern pthread_cond_t quota_cv;
/* Mutex to synchronize the server with the quota monitor */
extern pthread_mutex_t quota_mutex;
/* Command variable to run the quota monitor */
extern bool check_quota;

/* Mutex to synchronize the auto-test mode */
extern pthread_mutex_t test_mode_mutex;
extern bool test_mode_enabled;
#endif /* __WATCHER_GLOBAL__ */
