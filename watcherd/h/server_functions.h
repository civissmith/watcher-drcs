/*******************************************************************************
* @Title: server_functions.h
*
* @Author: Phil Smith
*
* @Date: Fri, 23-Dec-16 01:54PM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Expose the API within the server_functions module
*
*
*******************************************************************************/
#ifndef __WATCHERS_SERVER_FUNCTIONS__
#define __WATCHERS_SERVER_FUNCTIONS__
/***
 * Function: void* quota_monitor(void *args)
 * Inputs: void* args
 * Outputs: NONE
 * Returns: NONE
 * Description: This function runs as separate thread. It will monitor the
 * backing store to determine if it has gone over the quota. Once the quota is
 * exceeded, the oldest archive is removed.
 ***/
void *quota_monitor( void *args );

/***
 * Function: handle_connections(void* args)
 * Inputs: void* args
 * Outputs: NONE
 * Returns: NONE
 * Description: This function is a callback for handling incoming connections.
 ***/
void *handle_connection( void *args );
#endif                          /* __WATCHERS_SERVER_FUNCTIONS__ */
