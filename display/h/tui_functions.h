/*******************************************************************************
* @Title: display.h
*
* @Author: Phil Smith
*
* @Date: Tue, 31-Jan-17 08:10AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This module contains low-level APIs to query information about the
*           terminal's geometry.
*
*******************************************************************************/
#ifndef __WATCHER_TUI_FUNCTIONS__
#define __WATCHER_TUI_FUNCTIONS__
void clear_screen(  );
int get_window_size( struct screen_geometry_t *screen );
#endif                          /* __WATCHER_TUI_FUNCTIONS__ */
