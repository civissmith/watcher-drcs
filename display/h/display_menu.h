/*******************************************************************************
* @Title: menu.h
*
* @Author: Phil Smith
*
* @Date: Thu, 09-Mar-17 12:10PM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This header file exports the API to create a menu of items that can
*           be added to a display buffer.
*
*******************************************************************************/
#ifndef __WATCHER_MENU__
#define __WATCHER_MENU__

/***
 * struct MenuItem
 * Opaque type to contain information about the list elements of menu.
 ***/
typedef struct menu_item_t *MenuItem;

/***
 * struct Menu
 * Opaque type to contain information about the collected elements in a menu.
 ***/
typedef struct menu_t *Menu;


/***
 * Function: menu_create_item(char* s_name, char *d_name)
 * Inputs: char* s_name, char *d_name
 * Outputs: NONE
 * Returns: MenuItem
 * Description: Creates a new line item for a menu. The item will show S_NAME when
 *              the line is selected and D_NAME when de-selected.
 ***/
MenuItem menu_create_item( char *s_name, char *d_name );


/***
 * Function: menu_create_item()
 * Inputs: NONE
 * Outputs: NONE
 * Returns: Menu
 * Description: Creates an empty menu object. This object can then be populated
 *              with instances created with the menu_create_item API call.
 ***/
Menu menu_create_menu(  );


/***
 * Function: menu_add_item(Menu menu, MenuItem item)
 * Inputs: Menu menu, MenuItem item
 * Outputs: NONE
 * Returns: NONE
 * Description: Adds the ITEM to the MENU.
 ***/
void menu_add_item( Menu menu, MenuItem item );


/***
 * Function: destroy_menu(Menu menu)
 * Inputs: Menu menu
 * Outputs: NONE
 * Returns: NONE
 * Description: Deallcates the resources of the given menu. All menu items are
 *              also deallocted. This must be called when a menu is no longer
 *              needed or else memory leakage will occur.
 ***/
void destroy_menu( Menu menu );


/***
 * Function: menu_incr_cursor(Menu menu)
 * Inputs: Menu menu
 * Outputs: Menu
 * Returns: NONE
 * Description: Increments the cursor element of the provided menu. This method
 *              will automatically wrap the cursor if it would fall off the end
 *              of the list.
 ***/
void menu_incr_cursor( Menu menu );


/***
 * Function: menu_decr_cursor(Menu menu)
 * Inputs: Menu menu
 * Outputs: Menu
 * Returns: NONE
 * Description: Decrements the cursor element of the provided menu. This method
 *              will automatically wrap the cursor if it would fall off the
 *              beginning of the list.
 ***/
void menu_decr_cursor( Menu menu );


/***
 * Function: menu_get_names(Menu menu)
 * Inputs: Menu menu
 * Outputs: NONE
 * Returns: char* (or NULL when exhausted)
 * Description: This function is a generator function. Each successive call will
 *              return the next name in the list. Once the list is exhausted, it
 *              will return NULL.
 ***/
char *menu_get_names( Menu menu );


/***
 * Function: menu_get_selected_value(Menu menu, char* retval)
 * Inputs: Menu menu, char* retval
 * Outputs: Menu
 * Returns: NONE
 * Description: Populates the retval string with the S_NAME of the item that the
 *              menu's cursor is pointing to.
 ***/
void menu_get_selected_value( Menu menu, char *retval );


/***
 * Function: menu_get_selected_index(Menu menu)
 * Inputs: Menu menu
 * Outputs: NONE
 * Returns: int
 * Description: Given a Menu, return the numeric index of the selected item.
 ***/
int menu_get_selected_index( Menu menu );


/***
 * Function: menuitem_add_select_value(MenuItem item, char* string)
 * Inputs: MenuItem item, char* string
 * Outputs: NONE
 * Returns: NONE
 * Description: Adds the given string as the value parameter of the supplied item.
 *              Calls to menu_get_selected_value will return this value.
 ***/
void menuitem_add_select_value( MenuItem item, char *string );


/***
 * Function: menuitem_add_select_index(MenuItem item, int index)
 * Inputs: MenuItem item, int index
 * Outputs: NONE
 * Returns: NONE
 * Description: Assigns the given integer to the index parameter of the supplied
 *              item. Calls to menu_get_selected_index will return this value.
 ***/
void menuitem_add_select_index( MenuItem item, int index );

#endif                          /* __WATCHER_MENU__ */
