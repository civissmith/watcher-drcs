/*******************************************************************************
* @Title: display_pages.h
*
* @Author: Phil Smith
*
* @Date: Fri, 03-Mar-17 07:57AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This header file exports the API to create a displayable screen and
*           add content to display to the user.
*
*
*******************************************************************************/
#ifndef __WATCHER_DPAGES__
#define __WATCHER_DPAGES__
#include <stdlib.h>
#include "display_menu.h"

#define DEFAULT_LINES 20
#define MIN_ROW 18
#define MIN_COLUMN 50

struct screen_geometry_t {
    int rows;
    int columns;
};

/***
 * struct disp_screen_buffer_t
 * Opaque type to contain the information for the screen display buffer.
 ***/
typedef struct disp_screen_buffer_t *DispBuffer;

/***
 * Function: disp_deallocate_lines(DispBuffer screen)
 * Inputs: DispBuffer screen
 * Outputs: NONE
 * Returns: NONE
 * Description: Deallocates the resources for the given screen. This should be
 *              called when the display buffer is no longer needed. Failing to
 *              call this function will result in memory leakage.
 ***/
void disp_deallocate_lines( DispBuffer screen );

/***
 * Function: disp_add_line(DispBuffer screen, const char* data, size_t size)
 * Inputs: DispBuffer screen, const char* data, size_t size
 * Outputs: DispBuffer screen
 * Returns: NONE
 * Description: Adds SIZE bytes of DATA to the next line in the display buffer,
 *              SCREEN.
 ***/
void disp_add_line( DispBuffer screen, const char *data, size_t size );

/***
 * Function: disp_add_new_line(DispBuffer screen)
 * Inputs: DispBuffer screen
 * Outputs: DispBuffer screen
 * Returns: NONE
 * Description: Adds a blank line to the display buffer, SCREEN.
 ***/
void disp_add_new_line( DispBuffer screen );

/***
 * Function: disp_draw(DispBuffer screen)
 * Inputs: DispBuffer screen
 * Outputs: DispBuffer screen
 * Returns: NONE
 * Description: Draws the contents of the display buffer, SCREEN to the terminal.
 *              This function should be called once the display buffer has been
 *              fully populated.
 ***/
void disp_draw( DispBuffer screen );

/***
 * Function: disp_add_menu(DispBuffer screen, Menu menu)
 * Inputs: DispBuffer screen, Menu menu
 * Outputs: DispBuffer screen
 * Returns: NONE
 * Description: Adds the entire contents of MENU to the given display buffer,
 *              SCREEN.
 ***/
void disp_add_menu( DispBuffer screen, Menu menu );

/***
 * Function: disp_allocate_lines()
 * Inputs: NONE
 * Outputs: NONE
 * Returns: DispBuffer
 * Description: Allocates and initializes the memory for a display buffer. After
 *              this call, the SCREEN can have display lines or display menus
 *              added to it.
 ***/
DispBuffer disp_allocate_lines(  );

#endif                          /* __WATCHER_DPAGES__ */
