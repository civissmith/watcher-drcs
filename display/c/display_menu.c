/*******************************************************************************
* @Title: menu.c
*
* @Author: Phil Smith
*
* @Date: Thu, 09-Mar-17 12:09PM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This file contains the logic for the generic menu model.
*
*******************************************************************************/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "common.h"
#include "display_menu.h"


#define __CURSOR_SELECTED  "-> "
#define __CURSOR_DESELECTED  "   "
#define __CURSOR_LEN 4

/***
 * struct menu_t
 * Contains data required to manage the state of a selectable list menu.
 ***/
struct menu_t {
    MenuItem start;
    MenuItem cursor;
    MenuItem end;
    char cursor_selected[__CURSOR_LEN];
    char cursor_deselected[__CURSOR_LEN];
};

/***
 * struct menu_item_t
 * Encapsulates each individual element of a menu.
 ***/
struct menu_item_t {
    char deselected_name[NAME_LEN];
    char selected_name[NAME_LEN];
    char selected_text[LINE_LENGTH];
    int selected_index;
    bool selected;
    MenuItem prev;
    MenuItem next;
};


/***
 * Function: menu_create_menu()
 * Description: Allocates the resources for a menu instance. This function must
 *              be called before attempting to operate on a menu.
 ***/
Menu menu_create_menu(  )
{
    Menu menu = malloc( sizeof( struct menu_t ) );

    if ( menu == NULL ) {
        return NULL;
    }

    memset( menu, '\0', sizeof( struct menu_t ) );
    strncpy( menu->cursor_selected, __CURSOR_SELECTED, __CURSOR_LEN );
    strncpy( menu->cursor_deselected, __CURSOR_DESELECTED, __CURSOR_LEN );
    return menu;
}


/***
 * Function: menu_create_item(char* d_name, char* s_name)
 * Description: Allocates the resources for a menu element. The d_name will be
 *              displayed when the item "de-selected" and the s_name will be
 *              displayed when the item is "selected". This function must be
 *              called before any other operation on an item.
 ***/
MenuItem menu_create_item( char *d_name, char *s_name )
{
    MenuItem item = malloc( sizeof( struct menu_item_t ) );

    if ( item == NULL ) {
        return NULL;
    }
    memset( item, '\0', sizeof( struct menu_item_t ) );
    item->next = NULL;
    item->prev = NULL;
    item->selected = false;
    strncpy( item->deselected_name, d_name, NAME_LEN );
    if ( s_name != NULL ) {
        strncpy( item->selected_name, s_name, NAME_LEN );
    } else {
        strncpy( item->selected_name, d_name, NAME_LEN );
    }
    return item;
}


/***
 * Function: menu_add_item(Menu menu, MenuItem item)
 * Description: Associates the given item with the given menu.
 ***/
void menu_add_item( Menu menu, MenuItem item )
{

    /* Invariant: Menu cannot be null */
    if ( menu == NULL ) {
        return;
    }

    /* Invariant: Item cannot be null */
    if ( item == NULL ) {
        return;
    }

    /* The list was empty, so this is the first node */
    if ( menu->start == NULL ) {
        item->selected = true;
        item->next = NULL;
        item->prev = NULL;
        menu->start = item;
        menu->end = item;
        menu->cursor = item;
        return;
    }

    /* The menu already has a start, the next must be added to the free space */
    menu->end->next = item;
    item->prev = menu->end;
    item->next = NULL;
    menu->end = item;
}


/***
 * Function: destroy_menu(Menu menu)
 * Description: Frees the resources for the given menu. This function must be
 *              called when a menu is no longer needed otherwise memory leakage
 *              will occur.
 ***/
void destroy_menu( Menu menu )
{
    /* Invariant: Menu cannot be null */
    if ( menu == NULL ) {
        return;
    }

    /* Invariant: Menu start cannot be null */
    if ( menu->start == NULL ) {
        return;
    }

    MenuItem delete = menu->start;
    menu->end = NULL;
    menu->cursor = NULL;
    while ( delete != NULL ) {
        menu->start = menu->start->next;
        free( delete );
        delete = menu->start;
    }

    free( menu );
}


/***
 * Function: menu_incr_cursor(Menu menu)
 * Description: Increments the cursor of the given menu. This will cause the
 *              "next" element in the list to become "selected".
 ***/
void menu_incr_cursor( Menu menu )
{
    /* Invariant: Menu cannot be null */
    if ( menu == NULL ) {
        return;
    }
    /* Invariant: Menu->start cannot be null */
    if ( menu->start == NULL ) {
        return;
    }
    /* Invariant: Menu->end cannot be null */
    if ( menu->end == NULL ) {
        return;
    }

    /* Deselect the current item */
    menu->cursor->selected = false;

    if ( menu->cursor->next != NULL ) {
        /* Advance the cursor */
        menu->cursor = menu->cursor->next;
    } else {
        /* Wrap around the end */
        menu->cursor = menu->start;
    }

    /* Select the current item */
    menu->cursor->selected = true;
}


/***
 * Function: menu_decr_cursor(Menu menu)
 * Description: Decrements the cursor of the given menu. This will cause the
 *              "previous" item to become "selected".
 ***/
void menu_decr_cursor( Menu menu )
{
    /* Invariant: Menu cannot be null */
    if ( menu == NULL ) {
        return;
    }
    /* Invariant: Menu->start cannot be null */
    if ( menu->start == NULL ) {
        return;
    }
    /* Invariant: Menu->end cannot be null */
    if ( menu->end == NULL ) {
        return;
    }

    /* Deselect the current item */
    menu->cursor->selected = false;

    if ( menu->cursor->prev != NULL ) {
        /* Advance the cursor */
        menu->cursor = menu->cursor->prev;
    } else {
        /* Wrap around the end */
        menu->cursor = menu->end;
    }

    /* Select the current item */
    menu->cursor->selected = true;
}


/***
 * Function: menu_get_names(Menu menu)
 * Description: This function uses the "generator pattern" to return the names
 *              of items in the menu. For each call, return the next menu item's
 *              name or else NULL if the entire menu has been returned.
 ***/
char *menu_get_names( Menu menu )
{
    /* Invariant: Menu cannot be null */
    if ( menu == NULL ) {
        return NULL;
    }
    /* Invariant: Menu->start cannot be null */
    if ( menu->start == NULL ) {
        return NULL;
    }
    static MenuItem name_cursor = NULL;
    static char return_line[LINE_LENGTH] = { '\0' };
    memset(return_line, '\0', LINE_LENGTH);

    if ( name_cursor == NULL ) {
        name_cursor = menu->start;
        if ( name_cursor->selected ) {
            snprintf( return_line, LINE_LENGTH, "%s %s", menu->cursor_selected,
                      name_cursor->selected_name );
            return return_line;
        }
        snprintf( return_line, LINE_LENGTH, "%s %s", menu->cursor_deselected,
                  name_cursor->deselected_name );
        return return_line;
    }

    /* Move to the next element */
    name_cursor = name_cursor->next;

    /* Check for end of list */
    if ( name_cursor == NULL ) {
        return NULL;
    }

    if ( name_cursor->selected ) {
        snprintf( return_line, LINE_LENGTH, "%s %s", menu->cursor_selected,
                  name_cursor->selected_name );
        return return_line;
    }
    snprintf( return_line, LINE_LENGTH, "%s %s", menu->cursor_deselected,
              name_cursor->deselected_name );
    return return_line;
}


/***
 * Function: menu_get_selected_index(Menu menu)
 * Description: Returns the numeric index parameter of the item in the list that
 *              is "selected" at the time of the call.
 ***/
int menu_get_selected_index( Menu menu )
{
    /* Invariant: Menu cannot be null */
    if ( menu == NULL ) {
        return -1;
    }
    /* Invariant: Menu->start cannot be null */
    if ( menu->start == NULL ) {
        return -1;
    }

    menu->cursor = menu->start;
    while ( menu->cursor != NULL ) {
        if ( menu->cursor->selected ) {
            return menu->cursor->selected_index;
        }
        menu->cursor = menu->cursor->next;
    }

    /* No element was found to be selected. This is an error */
    return -2;
}


/***
 * Function: menu_get_selected_value(Menu menu, char* retval)
 * Description: Updates the in/out retval argument with the value parameter of
 *              the item that is "selected" at the time of the call. The retval
 *              parameter can be set to NULL if no value was set.
 ***/
void menu_get_selected_value( Menu menu, char *retval )
{
    /* Invariant: Menu cannot be null */
    if ( menu == NULL ) {
        return;
    }
    /* Invariant: Menu->start cannot be null */
    if ( menu->start == NULL ) {
        return;
    }
    /* Invariant: retval cannot be null */
    if ( retval == NULL ) {
        return;
    }

    menu->cursor = menu->start;
    while ( menu->cursor != NULL ) {
        if ( menu->cursor->selected && menu->cursor->selected_text != NULL ) {
            strncpy( retval, menu->cursor->selected_text, LINE_LENGTH );
            return;
        } else if ( menu->cursor->selected
                    && menu->cursor->selected_text == NULL ) {
            snprintf( retval, LINE_LENGTH, "NO_SELECTED_TEXT" );
            return;
        }
        menu->cursor = menu->cursor->next;
    }

    /* No elements are selected. This is an error */
    snprintf( retval, LINE_LENGTH, "NO_ITEMS_SELECTED" );
    return;
}


/***
 * Function: menuitem_add_select_index(MenuItem item, int index)
 * Description: Adds the given index argument as the index parameter of the
 *              given menu item.
 ***/
void menuitem_add_select_index( MenuItem item, int index )
{

    /* Invariant: Item cannot be null */
    if ( item == NULL ) {
        return;
    }
    item->selected_index = index;
}


/***
 * Function: menuitem_add_select_value(MenuItem item, char* string)
 * Description: Adds the given string as the value parameter of the given menu
 *              item.
 ***/
void menuitem_add_select_value( MenuItem item, char *string )
{

    /* Invariant: Item cannot be null */
    if ( item == NULL ) {
        return;
    }
    strncpy( item->selected_text, string, LINE_LENGTH );
}
