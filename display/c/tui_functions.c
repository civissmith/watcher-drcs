/*******************************************************************************
* @Title: tui_functions.c
*
* @Author: Phil Smith
*
* @Date: Tue, 31-Jan-17 08:10AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Implements the TUI functions
*
*
*******************************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <termios.h>
#include <signal.h>
#include <unistd.h>
#ifndef TIOCGWINSZ
#include <sys/ioctl.h>
#endif
#include <stdlib.h>
#include <string.h>
#include "display_pages.h"

/***
 * Function: clear_screen()
 * Description: Clears the terminal text.
 ***/
void clear_screen(  )
{
    printf( "\033[2J\033[;H" );
}


/***
 * Function: get_window_size(struct screen_geometry_t* screen)
 * Description: Updates the in/out parameter SCREEN with the current size of
 *              the terminal (in rows and columns).
 ***/
int get_window_size( struct screen_geometry_t *screen )
{

    struct winsize size;

    if ( ioctl( STDIN_FILENO, TIOCGWINSZ, ( char * )&size ) < 0 ) {
        printf( "(tui_f) Error getting window size\n" );
        return -1;
    }
    screen->rows = size.ws_row;
    screen->columns = size.ws_col;
    return 0;
}
