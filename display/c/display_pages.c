/*******************************************************************************
* @Title: display_pages.c
*
* @Author: Phil Smith
*
* @Date: Tue, 31-Jan-17 08:10AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Implement an API to manage drawing elements on a terminal display.
*
*
*******************************************************************************/
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#ifndef TIOCGWINSZ
#include <sys/ioctl.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include "common.h"
#include "display_pages.h"
#include "tui_functions.h"
#include "display_menu.h"
#define LINE_ALLOC 2

#define PROGRAM_TITLE "Watcher DRCS\0"
#define PROGRAM_FOOTER "Lockheed Martin Corporation - 2017\0"

struct disp_screen_buffer_t {
    int padding;
    int lines_used;
    int lines_allocated;
    char **lines;
};

static int __compute_midpoint( int string_len, int line_len );
static void __set_cursor_pos( int row, int col );
static void __draw_banner(  );

extern char selected_store[];
extern int selected_port;

/******************************************************************************
 *                                 PUBLIC API                                 *
 ******************************************************************************/

/***
 * Function: disp_allocate_lines()
 * Description: Allocate the screen space for a page.
 ***/
DispBuffer disp_allocate_lines(  )
{

    int i = 0;

    /* Allocate space for the core structure */
    DispBuffer screen = malloc( sizeof( struct disp_screen_buffer_t ) );
    memset( screen, '\0', sizeof( struct disp_screen_buffer_t ) );

    /* Allocate space for the line buffers */
    screen->lines = malloc( LINE_ALLOC * sizeof( char * ) );

    /* Clear out the memory for each line */
    for ( i = 0; i < LINE_ALLOC; i++ ) {
        screen->lines[i] = malloc( LINE_LENGTH * sizeof( char ) );
        if ( screen->lines[i] == NULL ) {
            //TODO: Handle this error
        }
        memset( screen->lines[i], '\0', LINE_LENGTH );
    }

    /* Account for usage (include header and footer) */
    screen->lines_used = 0;
    screen->padding = 6;
    screen->lines_allocated = LINE_ALLOC;
    return screen;
}

/***
 * Function: disp_deallocate_lines(DispBuffer screen)
 * Description: Cleans up resources used by a screen buffer.
 ***/
void disp_deallocate_lines( DispBuffer screen )
{

    /* Invariant: screen cannot be NULL */
    if ( screen == NULL ) {
        return;
    }

    int i = 0;
    /* Deallocate the lines */
    for ( i = screen->lines_allocated - 1; i >= 0; i-- ) {
        free( screen->lines[i] );
    }
    free( screen->lines );

    /* Deallocate the core structure */
    free( screen );
}

/***
 * Function: disp_add_new_line()
 * Description: Add a blank line to a display buffer.
 ***/
void disp_add_new_line( DispBuffer screen )
{

    int index = screen->lines_used;

    /* The screen buffer is nearly full, allocate more space */
    if ( screen->lines_used >= screen->lines_allocated ) {

        screen->lines_allocated += LINE_ALLOC;
        char **new_buffer = realloc( screen->lines,
                                     screen->lines_allocated *
                                     sizeof( char * ) );
        if ( new_buffer == NULL ) {
            free( screen->lines );
        }
        screen->lines = new_buffer;

        /* Initialize the new data */
        int i = 0;
        for ( i = screen->lines_used; i < screen->lines_allocated; i++ ) {
            screen->lines[i] = malloc( LINE_LENGTH * sizeof( char ) );
            if ( screen->lines[i] == NULL ) {
                //TODO: Handle this error
            }
            memset( screen->lines[i], '\0', LINE_LENGTH );
        }
    }

    strncpy( screen->lines[index], "\n", 1 );
    /* Ensure the string gets terminated */
    screen->lines[index][2] = '\0';
    screen->lines_used++;

}

/***
 * Function: disp_add_line( DispBuffer screen, const char *data, size_t size)
 * Description: Add SIZE bytes of DATA to the SCREEN display buffer.
 ***/
void disp_add_line( DispBuffer screen, const char *data, size_t size )
{

    int index = screen->lines_used;
    /* Truncate lines longer than the limit */
    if ( size >= LINE_LENGTH ) {
        size = LINE_LENGTH - 1;
    }

    /* The screen buffer is nearly full, allocate more space */
    if ( screen->lines_used >= screen->lines_allocated ) {

        screen->lines_allocated += LINE_ALLOC;
        char **new_buffer = realloc( screen->lines,
                                     screen->lines_allocated *
                                     sizeof( char * ) );
        if ( new_buffer == NULL ) {
            free( screen->lines );
        }
        screen->lines = new_buffer;

        /* Initialize the new data */
        int i = 0;
        for ( i = screen->lines_used; i < screen->lines_allocated; i++ ) {
            screen->lines[i] = malloc( LINE_LENGTH * sizeof( char ) );
            if ( screen->lines[i] == NULL ) {
                //TODO: Handle this error
            }
            memset( screen->lines[i], '\0', LINE_LENGTH );
        }
    }
    strncpy( screen->lines[index], data, size );

    /* Ensure the string gets terminated */
    screen->lines[index][size + 1] = '\0';
    screen->lines_used++;
}

/***
 * Function: disp_add_menu(DispBuffer screen, Menu menu)
 * Description: Add MENU to the SCREEN display buffer.
 ***/
void disp_add_menu( DispBuffer screen, Menu menu )
{

    char *name = NULL;

    name = menu_get_names( menu );
    while ( name != NULL ) {
        disp_add_line( screen, name, strlen( name ) );
        name = menu_get_names( menu );
    }
}

/***
 * Function: disp_draw( DispBuffer screen )
 * Description: Output the SCREEN display buffer to the screen.
 ***/
void disp_draw( DispBuffer screen )
{
    clear_screen(  );
    __draw_banner(  );

    int index = 0;
    int row = 5;
    int retval;
    int mid_point = 0;
    char message[LINE_LENGTH] = { '\0' };

    struct screen_geometry_t geometry = {.rows = 0,.columns = 0 };
    retval = get_window_size( &geometry );
    if ( retval < 0 ) {
        fprintf( stderr, "Failed to get screen geometry\n" );
        exit( EXIT_FAILURE );
    }

    if ( ( screen->lines_used + screen->padding ) > geometry.rows ) {
        snprintf( message, LINE_LENGTH, "------- %d LINES TRUNCATED -------",
                  ( screen->lines_used + screen->padding - geometry.rows +
                    2 ) );
        for ( index = 0; index < geometry.rows - screen->padding - 2; index++ ) {
            __set_cursor_pos( row++, 0 );
            printf( "    %s", screen->lines[index] );
            fflush( stdout );
        }
        mid_point = __compute_midpoint( strlen( message ), geometry.columns );
        row += 1;
        __set_cursor_pos( row, mid_point );
        printf( "%s", message );
        fflush( stdout );
    } else {
        for ( index = 0; index < screen->lines_used; index++ ) {
            __set_cursor_pos( row++, 0 );
            printf( "    %s", screen->lines[index] );
            fflush( stdout );
        }
    }
    __set_cursor_pos( 0, 0 );
}

/******************************************************************************
 *                        NON-PUBLIC HELPER FUNCTIONS                         *
 ******************************************************************************/

/***
 * Function: __compute_midpoint(int str_len, int lin_len)
 * Description: Compute the point of a line LIN_LEN chars long such that
 *              a string STR_LEN chars long will be centered.
 ***/
static int __compute_midpoint( int string_len, int line_len )
{
    int mid_line = line_len / 2;
    int mid_string = string_len / 2;

    return mid_line - mid_string;
}

/***
 * Function: __draw_banner()
 * Description: Print the program banner to the display.
 ***/
void __draw_banner(  )
{
    struct screen_geometry_t screen = {.rows = 0,.columns = 0 };
    int retval = 0;
    int count = 0;
    int mid_point = 0;

    // Get the screen geometry for this pass
    retval = get_window_size( &screen );
    if ( retval < 0 ) {
        fprintf( stderr, "Failed to get screen geometry\n" );
        exit( EXIT_FAILURE );
    }

    mid_point = __compute_midpoint( strlen( PROGRAM_TITLE ), screen.columns );

    // Draw the top line
    __set_cursor_pos( 1, 0 );
    for ( count = 0; count < screen.columns; count++ ) {
        printf( "*" );
    }

    // Draw the text title
    __set_cursor_pos( 2, mid_point );
    printf( "%s", PROGRAM_TITLE );

    // Draw the bottom line
    __set_cursor_pos( 3, 0 );
    for ( count = 0; count < screen.columns; count++ ) {
        printf( "*" );
    }

    __set_cursor_pos( 4, 0 );
    printf( "Backing Store: %s", selected_store );
    __set_cursor_pos( 4, 40 );
    printf( "Port: %d", selected_port );

    // Draw the origin line
    __set_cursor_pos( screen.rows - 1, 0 );
    mid_point = __compute_midpoint( strlen( PROGRAM_FOOTER ), screen.columns );
    __set_cursor_pos( screen.rows - 1, mid_point );
    printf( "%s", PROGRAM_FOOTER );
}

/***
 * Function: __set_cursor_pos(int row, int col)
 * Description: Move the cursor to row ROW and column COL.
 ***/
static void __set_cursor_pos( int row, int col )
{

    char tmpStr[LINE_LENGTH] = { '\0' };

    sprintf( tmpStr, "\033[%d;%dH", row, col );
    printf( "%s", tmpStr );
}
