#!/usr/bin/python
################################################################################
# @Title: server_test.py
#
# @Author: Phil Smith
#
# @Date: Fri, 17-Mar-17 04:31AM
#
# @Project: DC-K-00661 - Enhanced Backup Scripts
#
# @Purpose: Test harness to prove behavior of the watcher server application.
#
#
################################################################################
from TestClient import TestClient
import os
import re
import sys
import time
import os.path as op
import shutil
import subprocess as sp

def check_server(ip="0.0.0.0",port=80):

    print "[*] Checking %s:%d"%(ip,port)

    client = TestClient(ip, port)

    # Note: The server will disconnect after rx'ing a command. So each call must
    #       reconnect before sending.

    # Put the server into test mode
    client.connect()
    client.socket.send("WHEEL_V2 TEST_START\r\n\r\n")
    time.sleep(2)

    # Get the stats for the server
    client.connect()
    client.socket.send("WHEEL_V2 STATS\r\n\r\n")

    msg = client.socket.recv(4096)
    msg = msg.replace("\n", "|")
    rx = msg
    while rx != "":
        rx = client.socket.recv(4096)
        msg += rx.strip()
        msg = msg.replace("\n", "|")

    # Pull out the different parameters
    msg = msg.split('|')
    for each in msg:
        if "quota" in each.lower():
            quota = int(each.split(":")[1])

    # Now send commands check the quota
    print "[*] Server Quota = %d"%(quota,)
    PADDING = 5

    # Pre-generate all of the test files
    files = ["watcher_archive_test_file%03d.tmp"%index for index in range(1,quota+PADDING+1)]
    generate_test_files(files)


    # First test, send 1 file and check that it made it
    print "[*] TEST #1: Send 1 file"
    time.sleep(2)
    client.connect()
    client.socket.send("WHEEL_V2 SEND %s %d\r\n\r\n"%(files[0], op.getsize(files[0])))
    _input = open(files[0], "r")
    for line in _input:
        client.socket.send("%s"%line)

    print "[*] TEST #1: Send complete. Checking reception"
    time.sleep(2)
    client.connect()
    client.socket.send("WHEEL_V2 LIST\r\n\r\n")
    msg = client.socket.recv(4096)
    rx = msg
    while rx != "":
        rx = client.socket.recv(4096)
        msg += rx.strip()
        msg = msg.replace("\n","|")
    results = msg.split("|")[:-1]
    if files[0] in results and len(results) == 1:
        print "[+] TEST #1: PASSED"
    else:
        print "[!] TEST #1: FAILED"

    # Check for quota expiration
    print "[*] TEST #2: Checking quota expiration"
    for test_file in files:
        time.sleep(2)
        client.connect()
        client.socket.send("WHEEL_V2 SEND %s %d\r\n\r\n"%(test_file, op.getsize(test_file)))
        _input = open(test_file, "r")
        for line in _input:
            client.socket.send("%s"%line)
        client.socket.close()

    print "[*] TEST #2: Waiting for server to expire files"
    time.sleep(5)
    client.connect()
    client.socket.send("WHEEL_V2 LIST\r\n\r\n")
    msg = client.socket.recv(4096)
    rx = msg
    while rx != "":
        rx = client.socket.recv(4096)
        msg += rx
        msg = msg.replace("\n","|")
    results = msg.split("|")[:-1]

    failed = False
    # Check to make sure only the last files remain
    for test_file in files[PADDING:quota+PADDING+1]:
        if test_file not in results:
            failed = True

    if len(results) != quota:
        failed = True

    if failed:
        print "[!] TEST #2: FAILED"
    else:
        print "[+] TEST #2: PASSED"

    # Return the system back to normal ops
    client.connect()
    client.socket.send("WHEEL_V2 TEST_STOP\r\n\r\n")

    # Cleanup the files
    print "[*] Cleaning up temporary files"
    for test_file in files:
        os.unlink(test_file)


def generate_test_files(files=[]):
    """
    Creates dummy files to send to the server under test
    """

    print "[*] Generating %d test files"%len(files)
    for each in files:
        try:
            _file = open(each, "w")
        except IOError:
            print "[-] FATAL ERROR: Could not generate test data."
            print "[-] Please check user has permissions to write files."
            sys.exit(1)
        _file.write("TEST FILE: %s\n"%each)
        _file.close()

def get_primary_info(name="./config"):
    """
    Reads the given configuration file and extracts the primary backing store
    IP and ports. Returns ip,port.
    """
    try:
        _input = open(name, 'r')
    except:
        print "Could not open %s for reading!"%name
        exit(1)

    for line in _input:
        if "primary_backup" in line.lower():
            ip_data = line.split("=")[-1]
            ip_name = ip_data.split(":")[0]
            ip_port = ip_data.split(":")[1]
    return ip_name, int(ip_port)

def disable_backups(crontab=""):
    """
    Disables the automatic client backups by commenting them out in the crontab.
    """
    if op.isfile(crontab):
        print "[*] Suspending automatic backups"

        crontab_bak = crontab + ".bak"
        crontab_tmp = crontab + ".tmp"
        shutil.copy(crontab, crontab_bak)

        src_cron = open(crontab, 'r')
        dst_cron = open(crontab_tmp, 'w')

        for line in src_cron:

            # Ignore lines not tagged as client data
            if "watcher_client" not in line:
                dst_cron.write(line)
                continue

            client_match = re.search("(\s*.*?\s+)(.*?watcher .*)", line)
            if client_match:
                cancel_task = "#%s%s\n"%(client_match.group(1), client_match.group(2))
                dst_cron.write(cancel_task)
            else:
                dst_cron.write(line)

        shutil.move(crontab_tmp, crontab)

    src_cron.close()
    dst_cron.close()

def wait_for_backup():
    """
    Suspends until no more client applications are running.
    """
    print "[*] Waiting for pending automatic backups"

    wait = True

    while wait:

        wait = False
        command = "ps aux"
        proc = sp.Popen(command, shell=True, stdout=sp.PIPE)

        output = proc.stdout.readlines()

        for line in output:
            if "watcher -a" in line:
                wait = True
                time.sleep(5)


def enable_backups(cron=""):
    """
    Disables the automatic client backups by commenting them out in the crontab.
    """
    if op.isfile(crontab):
        print "[*] Re-enabling automatic backups"
        crontab_bak = crontab + ".bak"
        shutil.copy(crontab_bak, crontab)


if __name__ == '__main__':

    if len(sys.argv) != 2:
        print "usage: server_test <config file>"
        sys.exit(1)
    config = sys.argv[1]
    ip, port = get_primary_info(config)

    crontab = "/var/spool/cron/root"

    disable_backups(crontab)
    wait_for_backup()
    check_server(ip, port)
    enable_backups(crontab)
