#!/bin/bash
################################################################################
# @Title: deploy.sh
#
# @Author: Phil Smith
#
# @Date: Tue, 28-Feb-17 04:45AM
#
# @Project: DC-K-00661 - Enhanced Backup Scripts
#
# @Purpose: Push the software to the development RT host
#
#
################################################################################

# Make sure environment is set
if [[ $BUILD_BASE == "" ]]; then
    printf "Please source the environment setup script (env.sh) and try again\n"
    exit
fi

# Dev RT-Host IP address
target_ip0="192.168.56.200"
target_ip1="192.168.56.201"
target_ip2="192.168.56.202"

# Update the load on the rt_host dev PC
printf "Deploying to rt_host\n"
rsync -avz $BUILD_BASE root@$target_ip0: --exclude=bin --exclude=.git
rsync -avz $BUILD_BASE root@$target_ip1: --exclude=bin --exclude=.git
rsync -avz $BUILD_BASE root@$target_ip2: --exclude=bin --exclude=.git
