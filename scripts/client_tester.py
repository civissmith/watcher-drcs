#!/usr/bin/python
################################################################################
# @Title: client_tester.py
#
# @Author: Phil Smith
#
# @Date: Thu, 16-Mar-17 07:10AM
#
# @Project: DC-K-00661 - Enhanced Backup Scripts
#
# @Purpose: Run a mock server to intercept client requests and validate
#           protocol conformance.
#
#
################################################################################
from TestServer import TestServer

def run_test():
    """
    Launch a test server and wait for client to connect.
    """

    server = TestServer(port=50050)
    server.action = read_connection
    server.serve()

def read_connection(client=None, address=None):
    """
    Reads a connection and reports whether it passes protocol
    """
    rx_buffer = client.recv(4096)
    message = rx_buffer.split()

    # The key-up events will trigger an empty message
    if message == []:
        client.close()
        return

    protocol = message[0].upper()
    command  = message[1].upper()

    # Verify the protocol and parameters
    if protocol != "WHEEL_V2":
       print "Protocol validation failure. Test Failed."
       client.send("Invalid protocol\n")
       client.close()
       return

    if command == "LIST":
        client.send("Protocol: %s\n"%protocol)
        client.send("Command: %s\n"%command)
        client.send("Test: PASSED\n")
        client.close()
        print "List Test:    PASSED"
        return

    if command == "RESTORE":
        client.close()
        print "Restore Test: PASSED"
        return

    if command == "SEND":
        print "Send Test:    PASSED"
        client.close()
        return

    print "Test Failure. Invalid Command"
    client.close()

if __name__ == '__main__':
    run_test()
