#!/usr/bin/python3
################################################################################
# @Title: gen_files.py
#
# @Author: Phil Smith
#
# @Date: Mon, 26-Dec-16 11:25AM
#
# @Project: DCK-661
#
# @Purpose: Generate randomly filled files at 128MB in size
#
################################################################################
import sys
import subprocess as sp
import time

def gen_files(args=None):
    """Generate as many files as passed by the command line"""

    if len(args) != 2:
        print("Usage: gen_files.py count")
        exit(1)
    count = args[1]
    if not count.isdigit():
        print("Usage: gen_files.py count")
        exit(1)

    for number in range(1, int(count)+1):
        print("Creating watcher_archive_dummy{}.tar".format(number))
        file_ = open("watcher_archive_dummy{}.tar".format(number), 'w')
        file_.write("Hello World!\n")
        file_.close()
        time.sleep(2)

if __name__ == '__main__':
    gen_files(sys.argv)
