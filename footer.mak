################################################################################
# @Title: footer.mak
#
# @Author: Phil Smith
#
# @Date: Mon, 27-Feb-17 07:15AM
#
# @Project: DC-K-00661 - Enhanced Backup Scripts
#
# @Purpose: Contains the object generation rules for the build system
#
#
################################################################################
#
# Set the object names for the current module
#

#
# module_objs takes the source file name (with relative path from top), strips it
# to only the file name portion with no extension. Then it adds the ".o" suffix
# as well as the relative path to the output directory.
#
module_objs = $(addprefix $(object_dir)/,         \
              $(addsuffix .o,                     \
                  $(notdir $(basename $(source)))))

dependencies = $(addprefix $(object_dir)/,         \
              $(addsuffix .d,                      \
                  $(notdir $(basename $(source)))))

#
# Get which build rule to apply based on language of source files
#
extension=$(suffix $(word $(words $(source)), $(source)))

# C
ifeq (".c","$(extension)")

# Generate the object files (*.o)
$(object_dir)/%.o: $(subdirectory)/%.c $(object_dir)/.l
	@echo "Building $<"
	@$(CC) $(memsec) $(C_flags) $(include_flags) $< -o $@

# Generate the dependency files (*.d)
$(object_dir)/%.d: $(subdirectory)/%.c $(object_dir)/.l
	@$(CC) $(memsec) $(C_flags) $(include_flags) -M $< | \
   $(SED) 's,\($(notdir $*)\.o\) *:,$(dir $@)\1 $@: ,' > $@.tmp
	@$(MV) $@.tmp $@
endif


# Strip the path, 'lib' and '.so' from the library name to make a link target.
watcher_libs += $(library)
libraries += $(patsubst lib%,-l%,$(notdir $(basename $(library))))
objects   += $(object_dir)/*.o

# Filter out rt, rt_ctl and rt_rdr objects because they all define 'main'
$(library): $(module_objs) $(dependencies)
	@echo "Generating Library: $@"
	@$(AR) $(memsec) $(AR_flags) $@ $(filter-out %/watcher.o %/watcherd.o %.d,$^)


all::$(library)

# When not cleaning, also include the dependencies from the auto-gen dependency
# files.
ifneq ("$(MAKECMDGOALS)", "clean")
ifneq ("$(MAKECMDGOALS)", "install_watcherd")
ifneq ("$(MAKECMDGOALS)", "remove_watcherd")
ifneq ("$(MAKECMDGOALS)", "purge_watcherd")
  include $(dependencies)
endif
endif
endif
endif
