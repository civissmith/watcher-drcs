/*******************************************************************************
* @Title: exit_page.c
*
* @Author: Phil Smith
*
* @Date: Fri, 03-Feb-17 08:53AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This module contains the logic to allow the application to exit
*           gracefully. The exit command is a 2-state process.
*
*******************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include "display_pages.h"
#include "page_functions.h"

/***
 * Function: exit_page(struct page_args_t* args)
 * Description: Confirm the intent to exit the application.
 ***/
void exit_page( struct page_args_t *args )
{
    args->next_page = P_EXIT_CONFIRMED;
}
