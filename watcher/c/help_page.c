/*******************************************************************************
* @Title: help_page.c
*
* @Author: Phil Smith
*
* @Date: Fri, 03-Feb-17 07:33AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This module contains the logic to display the help menu. There are
*           no interactive components, it just shows what the commands on the
*           main page do.
*
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include "display_pages.h"
#include "page_functions.h"

#define HELP_LINE_1 "Help: Display this help page"
#define HELP_LINE_2 "Backup:"
#define HELP_LINE_3 "Initiate a manual backup of the data (does not override change requirement)"
#define HELP_LINE_4 "Restore Previous Archive:"
#define HELP_LINE_5 "Select an archive from chosen backing store and restore it to client"
#define HELP_LINE_6 "List Archives:"
#define HELP_LINE_7 "Display all of the archives stored in the selected backing store"
#define HELP_LINE_8 "->  Exit this page"

void help_page( struct page_args_t *args )
{
    int command = args->command;

    DispBuffer screen = disp_allocate_lines(  );

    /* The help page does not require any special formatting */
    disp_add_new_line( screen );
    disp_add_new_line( screen );
    disp_add_line( screen, HELP_LINE_1, strlen( HELP_LINE_1 ) );
    disp_add_new_line( screen );
    disp_add_line( screen, HELP_LINE_2, strlen( HELP_LINE_2 ) );
    disp_add_line( screen, HELP_LINE_3, strlen( HELP_LINE_3 ) );
    disp_add_new_line( screen );
    disp_add_line( screen, HELP_LINE_4, strlen( HELP_LINE_4 ) );
    disp_add_line( screen, HELP_LINE_5, strlen( HELP_LINE_5 ) );
    disp_add_new_line( screen );
    disp_add_line( screen, HELP_LINE_6, strlen( HELP_LINE_6 ) );
    disp_add_line( screen, HELP_LINE_7, strlen( HELP_LINE_7 ) );
    disp_add_new_line( screen );
    disp_add_new_line( screen );
    disp_add_new_line( screen );
    disp_add_line( screen, HELP_LINE_8, strlen( HELP_LINE_8 ) );
    disp_draw( screen );


    disp_deallocate_lines( screen );
    if ( command == CMD_ENTER ) {
        args->next_page = P_MAIN_MENU;
    }
}
