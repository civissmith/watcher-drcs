/*******************************************************************************
* @Title: automatic_mode.c
*
* @Author: Phil Smith
*
* @Date: Tue, 28-Feb-17 03:58AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Launch an backup task. This function is used in the automated system
*           but also serves as the back end for the user-commanded backups.
*
*******************************************************************************/
#include <dirent.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <syslog.h>
#include <unistd.h>
#include <unistd.h>
#include "client.h"
#include "client_functions.h"
#include "common.h"
#include "config.h"
#include "protocol.h"

#define COOKIE_FILE "watcher_restore_path"

/* Prototypes */
static long __get_file_size( FILE * file );

/***
 * Function: automatic_mode( char* config_name, bool interactive )
 * Description: Uses the supplied config file to learn backup parameters and
 *              performs a non-interactive backup (if necessary).
 ***/
void automatic_mode( char *config_name, bool interactive )
{

    struct config_t *config = NULL;

    /* Hook into the system logger */
    openlog( "watcher", LOG_PID, 0 );

    syslog( LOG_INFO, "Watcher client is running\n" );

    /* Setup the master configuration data structure */
    config = malloc( sizeof( struct config_t ) );
    if ( config == NULL ) {
        syslog( LOG_ERR, "Could not allocate configuration structure\n" );
        syslog( LOG_ERR, "Watcher client exited abnormally\n" );
        return;
    }
    memset( config, '\0', sizeof( struct config_t ) );

    /* Parse the configuration file and fill the config structure */
    parse_config( config_name, config );

    /* Ensure there are no high priority programs running */
    if ( !blacklist_clear( *config, interactive ) ) {
        destroy_config( config );
        return;
    }

    /* Since no blacklist tasks are running, continue with the backup */

    bool save_archive = false;
    bool primary_unavailable = false;
    char *hostname = config->primary_backup.host;
    char file_buffer[BUFSIZE] = { '\0' };
    int unavailable_alternates = 0;
    long file_size = 0;
    size_t bytes_read = 0;
    struct dir_entry_t *this_dir = NULL;
    char *start_dir = NULL;

    start_dir = getcwd(start_dir, PATH_MAX);
    if ( start_dir == NULL ){
        //TODO: Add error screen
        return;
    }

    /* Set the process to lower priority */
    nice( 5 );

    /* Iterate over the list of directories to determine and backup those that
     * have changed */
    config->target_dirs.cursor = config->target_dirs.head;
    while ( config->target_dirs.cursor != NULL ) {

        /* this_dir is just a helper to refer to the current directory */
        this_dir = config->target_dirs.cursor;

        /* Advance the directory list cursor now that there's a local reference
         * to the current directory */
        config->target_dirs.cursor = config->target_dirs.cursor->next;

        /* Skip the directory if it does not exist or can't be accessed */
        if ( chdir( this_dir->path_name ) < 0 ) {
            syslog( LOG_ERR, "Could not change to %s", this_dir->path_name );
            continue;
        }
        /* Else Operate on the directory */

        /* Write the directory's path to a path cookie so restore knows where
         * to put it. */
        FILE *cookie = NULL;
        cookie = fopen( COOKIE_FILE, "w" );
        fwrite( this_dir->path_name, strlen( this_dir->path_name ),
                sizeof( char ), cookie );
        fwrite( "\n", 1, sizeof( char ), cookie );
        fclose( cookie );

        /* Process the files in the current directory to see if any changed. If
         * at least 1 file changes, a backup will be requested */
        FileList *files = NULL;
        files = collect_files( this_dir->path_name, WATCHER_PREFIX );
        if ( files == NULL ) {
            syslog( LOG_ERR, "Could not collect files\n" );
            syslog( LOG_ERR, "Watcher client exited abnormally" );
            continue;
        }

        /* If no files have changed continue to the next directory in the
         * directory list */
        if ( !files_changed( files, CHECKSUM_FILE ) ) {
            destroy_file_list( files );
            continue;
        }

        char archive_prefix[NAME_MAX] = { '\0' };
        snprintf( archive_prefix, sizeof( archive_prefix ), "%s_%s",
                  WATCHER_PREFIX, this_dir->dir_name );
        char *archive_file = archive_files( files, archive_prefix );

        /* Gather file information in preparation for transmitting it */
        FILE *data_file = fopen( archive_file, "rb" );
        if ( data_file == NULL ) {
            syslog( LOG_ERR, "Could not open data file: %s\n", archive_file );
            syslog( LOG_ERR, "Watcher client exited abnormally" );
            destroy_file_list( files );
            continue;
        }
        file_size = __get_file_size( data_file );

        /* Create a client connection to the primary backing store in preparation
         * for transmitting the archive file. */
        unsigned short portno = config->primary_backup.port;
        Client primary_client = wa_get_client(  );
        wa_set_host( primary_client, hostname );
        wa_set_client_port( primary_client, portno );

        /* Establish comms with the primary backing store */
        if ( wa_connect( primary_client ) < 0 ) {
            syslog( LOG_ERR, "Host [%s:%hu] unavailable",
                    config->primary_backup.host, config->primary_backup.port );
            syslog( LOG_ERR, "Saving archive file locally" );
            primary_unavailable = true;
        }

        /* Form the watcher protocol backup 'SEND' */
        char header[BUFSIZE] = { '\0' };
        sprintf( header, "%s %s %ld\r\n\r\n", W_SEND, archive_file, file_size );

        /* Send the archive file */
        wa_transmit( primary_client, header, strlen( header ) );
        while ( !feof( data_file ) ) {

            memset( file_buffer, '\0', BUFSIZE );
            bytes_read =
                fread( file_buffer, sizeof( char ), BUFSIZE, data_file );

            if ( wa_transmit( primary_client, file_buffer, bytes_read ) < 0 ) {
                syslog( LOG_ERR,
                        "Failed to transmit all data to primary backup" );
                save_archive = true;
            }
        }

        /* Store the file checksums for future runs */
        save_checksums( files, CHECKSUM_FILE );

        /* Cleanup */
        wa_destroy_client( primary_client );
        destroy_file_list( files );
        fclose( data_file );

        /* If any alternates where defined, repeat the same process for transmitting
         * to send them the archive file. */
        if ( config->alternate_count > 0 ) {
            Client alternate_clients[config->alternate_count];
            int i = 0;

            /* Loop over every configured alternate */
            for ( i = 0; i < config->alternate_count; i++ ) {

                /* Create a client connection */
                alternate_clients[i] = wa_get_client(  );
                wa_set_host( alternate_clients[i],
                             config->alternate_backups[i].host );
                wa_set_client_port( alternate_clients[i],
                                    config->alternate_backups[i].port );

                /* Establish communications with the alternate backing store */
                if ( wa_connect( alternate_clients[i] ) < 0 ) {
                    syslog( LOG_WARNING,
                            "Alternate host (%s:%hu) unavailable\n",
                            config->alternate_backups[i].host,
                            config->alternate_backups[i].port );
                    wa_destroy_client( alternate_clients[i] );
                    unavailable_alternates += 1;
                    continue;
                }

                /* Form the watcher protocol backup 'SEND' */
                memset( header, '\0', sizeof( header ) );
                sprintf( header, "%s %s %ld\r\n\r\n", W_SEND, archive_file,
                         file_size );

                /* Send the file data */
                wa_transmit( alternate_clients[i], header, strlen( header ) );
                data_file = fopen( archive_file, "rb" );
                if ( data_file == NULL ) {
                    syslog( LOG_WARNING,
                            "Alternate could not open data file: %s\n",
                            archive_file );
                    wa_destroy_client( alternate_clients[i] );
                    continue;
                }
                while ( !feof( data_file ) ) {

                    memset( file_buffer, '\0', BUFSIZE );
                    bytes_read =
                        fread( file_buffer, sizeof( char ), BUFSIZE,
                               data_file );

                    if ( wa_transmit
                         ( alternate_clients[i], file_buffer,
                           bytes_read ) < 0 ) {
                        syslog( LOG_ERR,
                                "Failed to transmit all data to alternate backup\n" );
                    }
                }

                /* Cleanup */
                fclose( data_file );
                wa_destroy_client( alternate_clients[i] );
            }
        }

        /* Failure Mode: No available backing stores
         * Resolution: Save a local copy of the archive file */
        if ( primary_unavailable &&
             ( unavailable_alternates == config->alternate_count ) ) {
            save_archive = true;
        }

        if ( ( !save_archive ) ) {
            unlink( archive_file );
        }
    }                           // End Iterate over directories

    syslog( LOG_INFO, "Watcher client is finished\n" );
    destroy_config( config );

    /* Navigate back to the starting directory */
    chdir(start_dir);
    free(start_dir);
    start_dir = NULL;
}

/***
 * Function: __get_file_size(FILE* file)
 * Description: Return the size of the given file in bytes.
 ***/
static long __get_file_size( FILE * file )
{
    long retval = 0;
    long current_pos = 0;

    /* Save the current stream pos for restore */
    current_pos = ftell( file );

    fseek( file, 0, SEEK_END );

    retval = ftell( file );

    fseek( file, current_pos, SEEK_SET );

    return retval;
}
