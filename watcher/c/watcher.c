/*******************************************************************************
* @Title: watcher.c
*
* @Author: Phil Smith
*
* @Date: Wed, 14-Dec-16 01:36PM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: The top-level executable for the watcher client program. Decides
*           whether this is an interactive or automated transaction.
*
*
*******************************************************************************/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include "common.h"
#include "modes.h"

#define USAGE                                                                 \
"usage:\n"                                                                    \
"  watcher [options]\n"                                                       \
"options:\n"                                                                  \
"  -a                  Run the program in automatic mode\n"                   \
"  -c                  Configuration file (Default: ~/"DEFAULT_CONFIG")\n"    \
"  -h                  Show this help message\n"

/* Static bkp_config buffer is only used if no config file is given. This
 * buffer will contain the default config name.*/
static char bkp_config[PATH_MAX];
/***
 * Function: main(int args, char **argv)
 * Description: Parses the argument vector and determines the run-time mode.
 ***/
int main( int argc, char **argv )
{

    char option_char;
    char *config_name = NULL;
    bool interactive = true;

    /* Parse the command line arguments */
    while ( ( option_char = getopt( argc, argv, "c:ha" ) ) != -1 ) {
        switch ( option_char ) {
        case 'a':
            interactive = false;
            break;
        case 'c':
            config_name = optarg;
            break;
        case 'h':
            fprintf( stdout, "%s", USAGE );
            exit( EXIT_SUCCESS );
        default:
            fprintf( stderr, "%s", USAGE );
            exit( EXIT_FAILURE );
        }
    }

    /* Load the configuration file */
    if ( config_name == NULL ) {
        char* home = getenv("HOME");

        config_name = bkp_config;
        if( home == NULL ){
            fprintf(stderr, "Configuration ~/%s not loaded", DEFAULT_CONFIG);
            exit(EXIT_FAILURE);
        }
        snprintf(config_name, strlen(home)+2, "%s/", home);
        strncat(config_name, DEFAULT_CONFIG, strlen(DEFAULT_CONFIG));
    }

    /* Launch either interactive or automatic mode */
    if ( interactive ) {
        interactive_mode( config_name );
    } else {
        automatic_mode( config_name, false );
    }

    return EXIT_SUCCESS;
}
