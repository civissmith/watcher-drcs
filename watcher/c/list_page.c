/*******************************************************************************
* @Title: list_page.c
*
* @Author: Phil Smith
*
* @Date: Fri, 03-Feb-17 09:12AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Draw the page with the option to list the archives available on the
*           backing stores
*
*
*******************************************************************************/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "client.h"
#include "common.h"
#include "display_pages.h"
#include "page_functions.h"
#include "protocol.h"

#define PAGE_TITLE "Archive Listing:"
#define ERR_NO_STORE "Could not connect to backing store"

/***
 * Function: list_page(struct page_args_t* args)
 * Description: Lists the contents of the selected backing store. Sets the next_page
 *              field of the args structure to return to the menu.
 ***/
void list_page( struct page_args_t *args )
{
    static bool fetch_needed = true;
    static char *raw_buffer = NULL;
    static DispBuffer screen = NULL;

    int command = args->command;
    int start_offset = 0;
    int end_offset = 0;
    char header[BUFSIZE];
    char rx_buffer[BUFSIZE];
    char *archive_name = NULL;
    ssize_t rx_size = 0;

    memset( header, '\0', BUFSIZE );
    memset( rx_buffer, '\0', BUFSIZE );

    /* Create a client to fetch data from the server */
    Client client = wa_get_client(  );
    wa_set_host( client, args->target_store );
    wa_set_client_port( client, args->target_port );

    /* Make sure the backing store is reachable */
    if ( wa_connect( client ) < 0 ) {
        screen = disp_allocate_lines(  );
        disp_add_line( screen, "\n", 1 );
        disp_add_line( screen, "\n", 1 );
        disp_add_line( screen, ERR_NO_STORE, strlen( ERR_NO_STORE ) );
        disp_draw( screen );
        disp_deallocate_lines( screen );
        wa_destroy_client( client );
        args->next_page = P_EXIT;
        return;
    }

    /* This functions is called at each key press.
     * Only fetch data the first time */
    if ( fetch_needed ) {
        fetch_needed = false;

        /* Allocate a screen to display the page contents */
        screen = disp_allocate_lines(  );

        /* Allocate a buffer to hold all data coming back from the server */
        raw_buffer = malloc( BUFSIZE * sizeof( char ) );
        if ( raw_buffer == NULL ) {
            //TODO: Handle this error
        }
        memset( raw_buffer, '\0', BUFSIZE * sizeof( char ) );

        /* Start adding page text to the screen */
        disp_add_line( screen, "\n", strlen( "\n" ) );
        disp_add_line( screen, PAGE_TITLE, strlen( PAGE_TITLE ) );
        disp_add_line( screen, "\n", strlen( "\n" ) );

        /* Request the list from the backing store */
        memset( header, '\0', BUFSIZE );
        snprintf( header, BUFSIZE, "%s\r\n\r\n", W_LIST );
        wa_transmit( client, header, strlen( header ) );

        /* Receive the listing from the backing store */
        rx_size = wa_receive( client, rx_buffer, sizeof( rx_buffer ) );
        while ( rx_size > 0 ) {

            /* Determine where this message should fall in the raw data */
            start_offset = end_offset;
            end_offset = end_offset + rx_size;
            ssize_t new_size = end_offset + 1;

            /* Expand the buffer as new data arrives */
            char *new_buffer = realloc( raw_buffer, new_size );
            memset( new_buffer + start_offset, '\0',
                    ( new_size - start_offset ) );
            if ( new_buffer == NULL ) {
                free( raw_buffer );
            }

            /* Accumulate this message into the raw buffer */
            strncpy( new_buffer + start_offset, rx_buffer,
                     end_offset - start_offset );
            memset( rx_buffer, '\0', BUFSIZE );

            raw_buffer = new_buffer;
            rx_size = wa_receive( client, rx_buffer, sizeof( rx_buffer ) );
        }

        /* The data has been rx'ed from the backing store. Process it for display */
        archive_name = strtok( raw_buffer, "\n" );
        while ( archive_name != NULL ) {
            char line_buffer[LINE_LENGTH] = { '\0' };
            snprintf( line_buffer, LINE_LENGTH - 1, "    %s", archive_name );
            disp_add_line( screen, line_buffer, strlen( line_buffer ) );
            archive_name = strtok( NULL, "\n" );
        }

    }

    /* Draw the screen */
    if ( screen != NULL ) {
        disp_draw( screen );
    }

    /* Cleanup by destroying the client */
    wa_destroy_client( client );

    /* Handle key presses */
    if ( command == CMD_ESCAPE || command == CMD_ENTER ) {
        /* Either ESC or ENTER will return from the list */
        fetch_needed = true;
        args->next_page = P_MAIN_MENU;
        if ( raw_buffer != NULL ) {
            free( raw_buffer );
        }
        if ( screen != NULL ) {
            disp_deallocate_lines( screen );
        }
        return;
    }
}
