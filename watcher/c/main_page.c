/*******************************************************************************
* @Title: main_page.c
*
* @Author: Phil Smith
*
* @Date: Thu, 02-Feb-17 04:48AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Draws the main menu for the application. This menu is seen after
*           the backing store selection page. It functions as the UI to allow
*           users to select an operation to perform.
*
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "common.h"
#include "display_menu.h"
#include "display_pages.h"
#include "page_functions.h"

#define MENU_SIZE 5
extern char selected_store[];
extern int selected_port;

/***
 * data_t: Helper structure to organize menu data. Contains the name shown on
 *         the main menu when items are selected or deselected as well as the
 *         callback ID for the page function to call if selected.
 ***/
struct data_t {
    char deselect_name[NAME_LEN];
    char select_name[NAME_LEN];
    int page_id;
};

/***
 * Function: main_page(struct page_args_t *args)
 * Description: Draws the main menu page. Modifies the args->next_page parameter
 *              to allow page transitions.
 ***/
void main_page( struct page_args_t *args )
{
    static Menu action_menu = NULL;

    /* Create the elements to display in the menu.
     * Fields: {Deselect_name, Select_name, Page_ID}
     */
    struct data_t data[MENU_SIZE] = {
        {"Help", "HELP", P_HELP},
        {"Backup", "BACKUP", P_BACKUP_0},
        {"Restore Previous Archive", "RESTORE PREVIOUS ARCHIVE", P_RESTORE_0},
        {"List Archives", "LIST ARCHIVES", P_LIST_0},
        {"Exit", "EXIT", P_EXIT},
    };

    DispBuffer screen = NULL;
    int command = args->command;
    int next_page = 0;

    /* Allocate a screen buffer */
    screen = disp_allocate_lines(  );

    /* Handle page commands: */
    /* Up/Right Arrows */
    if ( command == CMD_UP || command == CMD_RIGHT ) {
        menu_decr_cursor( action_menu );
    }
    /* Down/Left Arrows */
    if ( command == CMD_DOWN || command == CMD_LEFT ) {
        menu_incr_cursor( action_menu );
    }
    /* Enter key */
    if ( command == CMD_ENTER ) {
        next_page = menu_get_selected_index( action_menu );

        if ( next_page < 0 ) {
            next_page = P_MAIN_MENU;
        }
        args->next_page = next_page;
        destroy_menu( action_menu );
        action_menu = NULL;
        disp_deallocate_lines( screen );
        return;
    }

    if ( command == CMD_ESCAPE  ){
        next_page = P_EXIT;
        args->next_page = next_page;
        destroy_menu( action_menu );
        action_menu = NULL;
        disp_deallocate_lines( screen );
        return;
    }

    /* Create a new action menu if this is the first run of the page */
    if ( action_menu == NULL ) {
        action_menu = menu_create_menu(  );
        MenuItem item = NULL;
        int index = 0;
        /* Process the menu template for display */
        for ( index = 0; index < MENU_SIZE; index++ ) {
            item = menu_create_item( data[index].deselect_name,
                                     data[index].select_name );
            menuitem_add_select_index( item, data[index].page_id );
            menu_add_item( action_menu, item );
        }
    }

    /* Pack the main menu text into the screen buffer */
    disp_add_new_line( screen );
    disp_add_line( screen, "Select", strlen( "Select" ) );
    disp_add_new_line( screen );
    disp_add_menu( screen, action_menu );

    /* Draw the screen buffer */
    disp_draw( screen );

    /* Cleanup */
    disp_deallocate_lines( screen );
}
