/*******************************************************************************
* @Title: backup_page.c
*
* @Author: Phil Smith
*
* @Date: Thu, 02-Feb-17 05:56AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This module contains the logic to display the user-commanded backup
*           page. A backup commanded in this fasion will still be subject to the
*           "change requirement", that is, there will have to be a modification
*           in the target directories for a backup to actually fire. Additionally
*           blacklist applications will also prevent a backup.
*
*******************************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include "display_pages.h"
#include "modes.h"
#include "page_functions.h"

#define PAGE_TITLE "Manual Backup Facility"
#define PAGE_CMD1  "->  Press ENTER to Backup"
#define PAGE_CMD2  "Backing up all contents"

/***
 * Function: backup_page(struct page_args_t* args)
 * Description: Draws the backup page. If the user presses "Enter", the system
 *              will attempt to backup the software.
 ***/
void backup_page( struct page_args_t *args )
{
    int command = args->command;

    DispBuffer screen = disp_allocate_lines(  );

    disp_add_line( screen, "\n", strlen( "\n" ) );
    disp_add_line( screen, "\n", strlen( "\n" ) );
    disp_add_line( screen, PAGE_TITLE, strlen( PAGE_TITLE ) );
    disp_add_line( screen, "\n", strlen( "\n" ) );
    disp_add_line( screen, PAGE_CMD1, strlen( PAGE_CMD1 ) );
    disp_draw( screen );

    if ( command == CMD_ENTER ) {
        disp_add_line( screen, "\n", strlen( "\n" ) );
        disp_add_line( screen, "\n", strlen( "\n" ) );
        disp_add_line( screen, PAGE_CMD2, strlen( PAGE_CMD2 ) );
        disp_draw( screen );

        // Perform the backup and return
        automatic_mode( args->config_name, true );
        args->next_page = P_MAIN_MENU;
    }
    if ( command == CMD_ESCAPE ) {
        args->next_page = P_MAIN_MENU;
    }
    disp_deallocate_lines( screen );
}
