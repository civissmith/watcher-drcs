/*******************************************************************************
* @Title: check_page.c
*
* @Author: Phil Smith
*
* @Date: Fri, 03-Feb-17 09:12AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose:
*
*
*******************************************************************************/
#include "display_pages.h"
#include "page_functions.h"
#include <stdio.h>
#include <unistd.h>

void check_page( struct page_args_t *args )
{
    args->next_page = P_MAIN_MENU;
}
