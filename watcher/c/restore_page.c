/*******************************************************************************
* @Title: restore_page.c
*
* @Author: Phil Smith
*
* @Date: Fri, 03-Feb-17 09:12AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This module contains the logic to display the archive restoration
*           page.
*
*******************************************************************************/
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "archive.h"
#include "client.h"
#include "config.h"
#include "common.h"
#include "display_menu.h"
#include "display_pages.h"
#include "page_functions.h"
#include "protocol.h"

#define ERR_NO_STORE "Could not connect to backing store"
#define PAGE_TITLE "Restore Archive:"
#define RESTORE_STATUS "Restoration in progress."

static int __request_file( const struct page_args_t *args,
                           const char *file_name );

/***
 * Function: restore_page(struct page_args_t* args)
 * Description: Draws the restoration page. Lists archive files available on the
 *              selected backing store.
 ***/
void restore_page( struct page_args_t *args )
{
    static bool fetch_needed = true;
    static char *raw_buffer = NULL;
    static char *page_buffer = NULL;
    static ssize_t total_size = 0;
    static Menu restore_menu = NULL;

    Client client = NULL;

    DispBuffer screen = NULL;
    int command = args->command;
    int start_offset = 0;
    int end_offset = 0;
    char header[BUFSIZE];
    char rx_buffer[BUFSIZE];
    char *archive_name = NULL;
    ssize_t rx_size = 0;
    memset( header, '\0', BUFSIZE );
    memset( rx_buffer, '\0', BUFSIZE );

    if ( restore_menu == NULL ) {
        restore_menu = menu_create_menu(  );
    }

    screen = disp_allocate_lines(  );
    disp_add_new_line( screen );
    disp_add_line( screen, PAGE_TITLE, strlen( PAGE_TITLE ) );
    disp_add_new_line( screen );

    if ( command == CMD_UP || command == CMD_RIGHT ) {
        menu_decr_cursor( restore_menu );
    }
    if ( command == CMD_DOWN || command == CMD_LEFT ) {
        menu_incr_cursor( restore_menu );
    }

    /* Read the config and get the list of backing stores */
    if ( command == CMD_ESCAPE ) {
        fetch_needed = true;
        args->next_page = P_MAIN_MENU;
        disp_deallocate_lines( screen );
        if ( raw_buffer != NULL ) {
            free( raw_buffer );
        }
        if ( restore_menu != NULL ) {
            destroy_menu( restore_menu );
            restore_menu = NULL;
        }
        return;
    }
    if ( command == CMD_ENTER ) {
        fetch_needed = true;

        disp_deallocate_lines( screen );
        char selected_name[LINE_LENGTH] = { '\0' };
        struct config_t *config = NULL;

        config = malloc( sizeof( struct config_t ) );
        if ( config == NULL ){
            //TODO: Handle this error
        }
        memset( config, '\0', sizeof(struct config_t));

        parse_config( args->config_name, config );

        DispBuffer progress_screen = disp_allocate_lines(  );
        disp_add_new_line(progress_screen);
        disp_add_new_line(progress_screen);
        disp_add_new_line(progress_screen);
        disp_add_line(progress_screen, RESTORE_STATUS, strlen(RESTORE_STATUS));
        disp_draw(progress_screen);

        menu_get_selected_value( restore_menu, selected_name );
        if ( __request_file( args, selected_name ) == 0 ) {
            if ( archive_unpack( selected_name, config->temp_dir ) == false ) {
/******************************************************************************
 !                           !!!!!   WARNING  !!!!!                           !
 ! This error condition has not been thoroughly tested. It will theoretically !
 ! behave if called, but there was no formal testing! File corruption         !
 ! may result!!                                                               !
 ******************************************************************************/
                DispBuffer error_screen = disp_allocate_lines(  );
                const char *error_message =
                    "ERROR: Could not unpack archive:\0";
                disp_add_new_line( error_screen );
                disp_add_line( error_screen, error_message,
                               strlen( error_message ) );
                disp_add_line( error_screen, selected_name,
                               strlen( selected_name ) );

                disp_draw( error_screen );
                sleep( 5 );
                disp_deallocate_lines( error_screen );
                unlink( selected_name );
            } else {
                unlink( selected_name );
            }
        }
        disp_deallocate_lines(progress_screen);
        destroy_config(config);
        args->next_page = P_MAIN_MENU;
        if ( raw_buffer != NULL ) {
            free( raw_buffer );
        }
        if ( restore_menu != NULL ) {
            destroy_menu( restore_menu );
            restore_menu = NULL;
        }
        return;
    }

    if ( fetch_needed ) {
        fetch_needed = false;

        client = wa_get_client(  );
        wa_set_host( client, args->target_store );
        wa_set_client_port( client, args->target_port );

        /* Make sure the backing store is reachable */
        if ( wa_connect( client ) < 0 ) {
            screen = disp_allocate_lines(  );
            disp_add_new_line( screen );
            disp_add_new_line( screen );
            disp_add_line( screen, ERR_NO_STORE, strlen( ERR_NO_STORE ) );
            disp_draw( screen );
            disp_deallocate_lines( screen );
            wa_destroy_client( client );
            args->next_page = P_EXIT;
            return;
        }

        raw_buffer = malloc( BUFSIZE * sizeof( char ) );
        if ( raw_buffer == NULL ) {
            //TODO: Handle this error
        }
        memset( raw_buffer, '\0', BUFSIZE * sizeof( char ) );

        /* Request the list from the backing store */
        memset( header, '\0', BUFSIZE );
        snprintf( header, BUFSIZE, "%s\r\n\r\n", W_LIST );
        wa_transmit( client, header, strlen( header ) );

        /* Receive the listing from the backing store */
        rx_size = wa_receive( client, rx_buffer, sizeof( rx_buffer ) );
        while ( rx_size > 0 ) {
            total_size += rx_size;
            /* Determine where this message should fall in the raw data */
            start_offset = end_offset;
            end_offset = end_offset + rx_size;
            char *new_buffer = realloc( raw_buffer, end_offset );
            if ( new_buffer == NULL ) {
                free( raw_buffer );
            }
            /* Accumulate this message into the raw buffer */
            strncpy( new_buffer + start_offset, rx_buffer,
                     end_offset - start_offset );
            memset( rx_buffer, '\0', BUFSIZE );
            raw_buffer = new_buffer;
            rx_size = wa_receive( client, rx_buffer, sizeof( rx_buffer ) );
        }
        wa_destroy_client( client );


        page_buffer = malloc( total_size + 1 );
        memset( page_buffer, '\0', total_size + 1 );
        memcpy( page_buffer, raw_buffer, total_size );

        /* The data has been rx'ed from the backing store. Process it for display */
        archive_name = strtok( page_buffer, "\n" );
        while ( archive_name != NULL ) {
            MenuItem item = menu_create_item( archive_name, NULL );
            menuitem_add_select_value( item, archive_name );
            menu_add_item( restore_menu, item );
            archive_name = strtok( NULL, "\n" );
        }

        free( page_buffer );
    }

    if ( screen != NULL ) {
        disp_add_menu( screen, restore_menu );
        disp_draw( screen );
        disp_deallocate_lines( screen );
    }

}

/***
 * Function: __request_file(const struct page_args_t* args, const char *file_name)
 * Description: Helper function to issue a request to selected backing store
 *              contained in the args to retrieve the given file name.
 ***/
static int __request_file( const struct page_args_t *args,
                           const char *file_name )
{

    Client client = NULL;
    client = wa_get_client(  );
    char *header = malloc( BUFSIZE );
    char rx_buffer[BUFSIZE];
    ssize_t rx_size = 0;
    memset( header, '\0', BUFSIZE );
    memset( rx_buffer, '\0', BUFSIZE );

    FILE *file;
    file = fopen( file_name, "wb" );
    wa_set_host( client, args->target_store );
    wa_set_client_port( client, args->target_port );
    wa_connect( client );

    snprintf( header, BUFSIZE, "%s %s\r\n\r\n", W_RESTORE, file_name );
    wa_transmit( client, header, strlen( header ) );

    rx_size = wa_receive( client, rx_buffer, sizeof( rx_buffer ) );
    while ( rx_size > 0 ) {
        fwrite( rx_buffer, sizeof( char ), rx_size, file );
        memset( rx_buffer, '\0', sizeof( rx_buffer ) );
        rx_size = wa_receive( client, rx_buffer, sizeof( rx_buffer ) );
    }

    wa_destroy_client( client );
    free( header );
    fclose( file );
    return rx_size;
}
