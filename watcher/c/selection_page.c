/*******************************************************************************
* @Title: selection_page.c
*
* @Author: Phil Smith
*
* @Date: Wed, 01-Mar-17 08:56AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Draws the selection menu to determine which machine is being
*           targeted.
*
*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "config.h"
#include "display_pages.h"
#include "page_functions.h"
#include "common.h"
#include "display_menu.h"


#define SELECT_TITLE "Choose a backing store. Press ESC to exit"

/* Prototypes */
void __set_menu_items( Menu menu, const struct config_t *config );
void __update_store_and_port( Menu menu, char ( *store )[NAME_LEN], int *port );

/* Program Global Variables */
char selected_store[NAME_LEN] = { '\0' };
char store_name[NAME_LEN] = { '\0' };
int selected_port = -1;


/***
 * Function: selection_page( page_args_t args )
 * Description: Draw the backing store selection page (landing page).
 ***/
void selection_page( struct page_args_t *args )
{

    static int selection = 0;
    static bool config_processed = false;
    static struct config_t *config;
    static Menu select_menu = NULL;

    DispBuffer screen = disp_allocate_lines(  );

    int retval = 0;
    char *config_name = args->config_name;
    int command = args->command;

    if ( !config_processed ) {

        /* Create a menu to store the backing store list */
        select_menu = menu_create_menu(  );

        /* Set aside space for a configuration structure */
        config = malloc( sizeof( struct config_t ) );
        config_processed = true;

        /* Read the configuration file into memory */
        retval = parse_config( config_name, config );
        if ( retval < 0 ) {

            //TODO: Update this to follow the "Error Screen" model
            /* In case of a configuration error, exit gracefully */
            args->next_page = P_EXIT_CONFIRMED;
            sleep( 5 );
            config_processed = false;
            destroy_config( config );
            destroy_menu( select_menu );
            return;
        }

        /* Add the data to the menu */
        __set_menu_items( select_menu, config );
    }

    /* Handle arrow commands */
    if ( command == CMD_UP || command == CMD_RIGHT ) {
        menu_decr_cursor( select_menu );
        selection -= 1;
        if ( selection < 0 ) {
            selection = config->alternate_count;
        }
    }

    if ( command == CMD_DOWN || command == CMD_LEFT ) {
        menu_incr_cursor( select_menu );
        selection = ( selection + 1 ) % ( config->alternate_count + 1 );
    }

    /* Handle the ENTER key */
    if ( command == CMD_ENTER ) {

        args->next_page    = P_MAIN_MENU;
        args->target_store = selected_store;
        args->target_port  = selected_port;

        char output[LINE_LENGTH] = { '\0' };
        menu_get_selected_value( select_menu, output );

        /* Cleanup before leaving the page */
        destroy_config( config );
        destroy_menu( select_menu );
        disp_deallocate_lines( screen );
        return;
    }

    /* Handle the ESC key */
    if ( command == CMD_ESCAPE ) {

        if ( config != NULL ) {
            destroy_config( config );
        }

        /* Cleanup before leaving the page */
        destroy_menu( select_menu );
        disp_deallocate_lines( screen );
        args->next_page = P_EXIT_CONFIRMED;
        return;
    }

    /* Draw the prompts */
    disp_add_new_line( screen );
    disp_add_line( screen, SELECT_TITLE, strlen( SELECT_TITLE ) );
    disp_add_new_line( screen );
    disp_add_menu( screen, select_menu );

    /* Updated the selected store and port */
    __update_store_and_port( select_menu, &selected_store, &selected_port );

    /* Draw the display to the screen */
    disp_draw( screen );
    disp_deallocate_lines( screen );
}

/***
 * Function: __update_store_and_port( Menu menu, char (*store)[], int *port )
 * Description: Updates the selected backing store and port in the display screen.
 ***/
void __update_store_and_port( Menu menu, char ( *store )[NAME_LEN], int *port )
{

    char temp_buffer[LINE_LENGTH] = { '\0' };
    char *temp_string = NULL;

    menu_get_selected_value( menu, temp_buffer );
    temp_string = strtok( temp_buffer, ":" );
    if ( temp_string != NULL ) {
        strncpy( *store, temp_string, NAME_LEN );
    }
    temp_string = strtok( NULL, ":" );
    if ( temp_string != NULL ) {
        *port = atoi( temp_string );
    } else {
        *port = -1;
    }
}

/***
 * Function: __set_menu_items( Menu menu, const config_t config )
 * Description: Add each backing store to the menu for display.
 ***/
void __set_menu_items( Menu menu, const struct config_t *config )
{

    char temp_buffer[LINE_LENGTH] = { '\0' };
    char temp_select[LINE_LENGTH] = { '\0' };
    int index = 0;
    MenuItem item = NULL;

    for ( index = 0; index < config->alternate_count + 1; index++ ) {
        if ( index != 0 ) {
            snprintf( temp_buffer, sizeof( temp_buffer ), "Alt:   %s:%d",
                      config->alternate_backups[index - 1].host,
                      config->alternate_backups[index - 1].port );
            item = menu_create_item( temp_buffer, NULL );
        } else {
            snprintf( temp_buffer, sizeof( temp_buffer ), "Prime: %s:%d",
                      config->primary_backup.host,
                      config->primary_backup.port );
            item = menu_create_item( temp_buffer, NULL );
        }
        snprintf( temp_select, sizeof( temp_select ), "%s:%d",
                  config->alternate_backups[index - 1].host,
                  config->alternate_backups[index - 1].port );
        menuitem_add_select_value( item, temp_select );
        menu_add_item( menu, item );
    }
}
