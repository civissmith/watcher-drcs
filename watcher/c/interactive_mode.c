/*******************************************************************************
* @Title: interactive_mode.c
*
* @Author: Phil Smith
*
* @Date: Tue, 28-Feb-17 03:58AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This module contains the logic to manage keyboard inputs and the state
*           of the application to allow users to navigate through the text
*           interface.
*
*******************************************************************************/
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include "page_functions.h"
#include "display_pages.h"
#include "tui_functions.h"

static struct termios save_termios;

/* MINBYTES: Determines how many bytes must be read for each key press.
 *           Arrow keys encode to 3 bytes. */
#define MINBYTES 3

int process_keys(  );
void set_term_discipline(  );
/* Create the jump table for each callable page */
static struct page_t page_table[] = {
    {P_HELP, help_page},
    {P_SELECTION_MENU, selection_page},
    {P_MAIN_MENU, main_page},
    {P_BACKUP_0, backup_page},
    {P_RESTORE_0, restore_page},
    {P_LIST_0, list_page},
    {P_EXIT, exit_page},
    {-1, NULL},
};


/***
 * Function: interactive_mode(char* config_name)
 * Description: Top-level method for the interactive session. Manages the state
 *              of the application by calling the appropriate callback for each
 *              page.
 ***/
void interactive_mode( char *config_name )
{

    struct screen_geometry_t screen = {.rows = 0,.columns = 0 };
    int retval = 0;
    int command_lp = 1;
    bool running = true;
    struct page_args_t page_args;
    page_args.next_page = P_SELECTION_MENU;

    retval = get_window_size( &screen );
    if ( retval < 0 ) {
        printf( "(Main) Error getting window size\n" );
        exit( EXIT_FAILURE );
    }
    if ( screen.rows < MIN_ROW || screen.columns < MIN_COLUMN ) {
        clear_screen(  );
        printf( "Please make screen at least %d x %d!\n", MIN_ROW, MIN_COLUMN );
        printf( "Screen: %d x %d\n", screen.rows, screen.columns );
        exit( EXIT_FAILURE );
    }


    set_term_discipline(  );

    /* The run-time loop */
    while ( running ) {

        /* Only update the screen if there was a command */
        if ( page_args.command != command_lp ) {
            /* Refresh the screen */

            /* Call the current page to draw it's display */
            page_args.config_name = config_name;
            page_table[page_args.next_page].display( &page_args );
            /* Flush stdout to make sure the page is displayed */
            fflush( stdout );
            if ( page_args.next_page == P_EXIT_CONFIRMED
                 || page_args.command == CMD_QUIT ) {
                running = false;
                break;
            }
        }
        command_lp = page_args.command;
        page_args.command = process_keys(  );
        usleep( 100 );

    }

    printf( "\033[2J\033[;H" );
    fflush( stdout );
    if ( tcsetattr( STDIN_FILENO, TCSAFLUSH, &save_termios ) < 0 ) {
        printf( "Could not set terminal correctly.\n" );
        exit( EXIT_FAILURE );
    }
}

/***
 * Function: process_keys()
 * Description: Manage reading from the terminal in raw mode and parse out the
 *              key codes from the keyboard. Report back an encoded value for
 *              which key was pressed.
 ***/
int process_keys(  )
{

    int index;
    int raw_command = 0;
    char command_buf[MINBYTES];

    /* Clear the buffer each time */
    memset( command_buf, '\0', MINBYTES );

    /* Block waiting for a key press */
    raw_command = read( STDIN_FILENO, &command_buf, MINBYTES );
    if ( raw_command == -1 ) {
        printf( "Read error!\n" );
        return -1;
    }

    /* Catch the UP arrow */
    if ( !( strncmp( command_buf, "\033[A", MINBYTES ) ) ) {
        return CMD_UP;
    }
    /* Catch the DOWN arrow */
    if ( !( strncmp( command_buf, "\033[B", MINBYTES ) ) ) {
        return CMD_DOWN;
    }
    /* Catch the RIGHT arrow */
    if ( !( strncmp( command_buf, "\033[C", MINBYTES ) ) ) {
        return CMD_RIGHT;
    }
    /* Catch the LEFT arrow */
    if ( !( strncmp( command_buf, "\033[D", MINBYTES ) ) ) {
        return CMD_LEFT;
    }

    for ( index = 0; index < MINBYTES; index++ ) {
        /* Set the cursor before writing characters */
        if ( command_buf[index] >= 0x30 && command_buf[index] <= 0x39 ) {
            return atoi( &command_buf[index] );
        } else if ( command_buf[index] == 0x0D ) {
            /* The enter key has been pressed, take action */
            return CMD_ENTER;
        } else if ( command_buf[index] == 0x1B ) {
            /* The escape key has been pressed, take action */
            return CMD_ESCAPE;
        }
        fflush( stdout );
    }
    return 0;
}

/***
 * Function: set_term_discipline()
 * Description: Change the terminal to raw mode so that the text interface can
 *              receive keyboard inputs.
 ***/
void set_term_discipline(  )
{
    struct termios buffer;
    memset( &buffer, '\0', sizeof( buffer ) );

    /* Save the current terminal setting so they can be restored on exit. */
    if ( tcgetattr( STDIN_FILENO, &save_termios ) < 0 ) {
        printf( "Could not save terminal settings...\n" );
        exit( EXIT_FAILURE );
    }

    cfmakeraw( &buffer );

    /* Set input to a polling read (ref: termios(3)) */
    buffer.c_cc[VMIN] = 0;
    buffer.c_cc[VTIME] = 0;
    if ( tcsetattr( STDIN_FILENO, TCSAFLUSH, &buffer ) < 0 ) {
        printf( "Could not set terminal correctly.\n" );
        exit( EXIT_FAILURE );
    }

    if ( ( buffer.c_lflag & ( ECHO | ICANON ) ) || buffer.c_cc[VMIN] != 0
         || buffer.c_cc[VTIME] != 0 ) {
        printf( "Terminal not set correctly.\n" );
        exit( EXIT_FAILURE );
    }
}
