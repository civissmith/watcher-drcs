/*******************************************************************************
* @Title: archive.c
*
* @Author: Phil Smith
*
* @Date: Tue, 14-Mar-17 10:36AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This module contains the logic for a simple archiver API. The functions
*           contained here provide a way for an application to unpack an archive.
*
*******************************************************************************/
#define _XOPEN_SOURCE 500
#include <ftw.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include "archive.h"
#include "common.h"
#include "display_pages.h"
#include "file.h"

#define MANIFEST_FILE "watcher_restore_path"
#define TEMP_DIR_MODE 0755


/***
 * Function: get_file_path(char* buffer)
 * Description: Given a buffer containing a file name, modify the buffer to
 *              contain the path to the buffer. NOTE: The single parameter is
 *              an IN-OUT parameter!
 ***/
bool get_file_path( char *buffer )
{
    char input[BUFSIZE] = { '\0' };

    /* Save the path to the manifest file */
    strncpy( input, buffer, sizeof( input ) );

    FILE *manifest = fopen( input, "r" );
    if (manifest == NULL){
        return false;
    }

    fgets( buffer, BUFSIZE, manifest );
    buffer = strtok( buffer, "\n" );
    fclose( manifest );
    return true;
}

/***
 * Function: archive_unpack(const char* archive, const char* temp_dir)
 * Description: Given the path to an archive file, unpack the archive into the
 *              temporary directory. Then restore its contents to its manifest's
 *              location.
 *              Return false if any of these steps fail.
 ***/
bool archive_unpack( const char *archive, const char *temp_dir )
{

    /* Invariant: archive cannot be NULL */
    if ( archive == NULL ) {
        return false;
    }
    int retval = 0;

    char temp_buffer[BUFSIZE] = { '\0' };

    /* Remove the old temp directory */
    file_remove_dir( temp_dir );

    /* Create the temporary directory */
    if ( mkdir( temp_dir, TEMP_DIR_MODE ) ) {
        return false;
    }

    snprintf( temp_buffer, sizeof( temp_buffer ), "tar -xf %s -C %s", archive,
              temp_dir );
    retval = system( temp_buffer );

    if ( retval != 0 ){
        DispBuffer error_screen = disp_allocate_lines(  );
        const char *error_message = "ERROR> Could not unpack archive:\0";
        disp_add_new_line( error_screen );
        disp_add_line( error_screen, error_message,
                       strlen( error_message ) );
        disp_add_line( error_screen, archive, strlen( archive ) );
        disp_draw( error_screen );
        sleep( 5 );
        disp_deallocate_lines( error_screen );
        return false;
    }

    snprintf( temp_buffer, sizeof( temp_buffer ), "%s/%s", temp_dir,
              MANIFEST_FILE );
    retval = get_file_path( temp_buffer );
    /* Warn the user if the manifest file could not be opened for some
     * reason. */
    if ( retval == false ){
        DispBuffer error_screen = disp_allocate_lines(  );
        const char *error_message = "ERROR> Manifest file missing from archive:\0";
        disp_add_new_line( error_screen );
        disp_add_line( error_screen, error_message,
                       strlen( error_message ) );
        disp_add_line( error_screen, archive, strlen( archive ) );
        disp_draw( error_screen );
        sleep( 5 );
        disp_deallocate_lines( error_screen );
        return false;
    }

    /* temp_buffer now contains the path where files should be restored */

    /* Remove the directory before re-populating it */
    file_remove_dir( temp_buffer );

    /* Move the contents from the temp dir to the restore dir */
    rename( temp_dir, temp_buffer );

    return true;
}
