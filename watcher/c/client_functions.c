/*******************************************************************************
* @Title: client_functions.c
*
* @Author: Phil Smith
*
* @Date: Tue, 20-Dec-16 07:50AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This module contains functions required to support the automated
*           backup functions.
*
*******************************************************************************/
#include <dirent.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>
#include "client_functions.h"
#include "common.h"
#include "display_pages.h"
#include "file.h"


static void __reset_file_list( FileList * files );
static void __get_time_of_day( char *date_string );

/***
 * struct file_node_t:
 * Representation of a file on disk with meta-data to tie into a linked-list
 * of files.
 ***/
struct file_node_t {
    char name[TINYBUF];
    char checksum[TINYBUF];
    size_t size;
    struct file_node_t *next;
};

/***
 * struct file_list_t:
 * A linked-list made of file_node_t files.
 ***/
struct file_list_t {
    struct file_node_t *head;
    struct file_node_t *cursor;
    int size;
};


/***
 * Function: FileList* collect_files(char* path, const char* prefix)
 * Inputs:
 *  char* path - path to directory that contains files to be collected
 *  char* prefix - identifier for files that should not be included in list
 * Outputs: NONE
 * Returns:
 *  FileList* - success
 *  NULL - failure
 * Description: This function returns a list of files in the directory specified
 * by PATH. Any file that begins with PREFIX is ignored. The returned file list
 * should be destroyed to prevent memory leaks.
 ***/
FileList *collect_files( char *path, const char *prefix )
{

    struct stat statbuf;

    /* Invariant: Path must point to a readable directory */
    stat( path, &statbuf );
    if ( !S_ISDIR( statbuf.st_mode ) ) {
        fprintf( stderr, "(%s) is not a readable directory\n", path );
        return NULL;
    }

    DIR *dir_ptr = opendir( path );
    struct dirent *dir_entry;
    FILE *raw_checksum = NULL;
    char command[TINYBUF] = { '\0' };
    char checksum_string[SMALLBUF] = { '\0' };
    char *checksum = NULL;

    /* Create the file list in memory */
    FileList *file_list = NULL;
    file_list = malloc( sizeof( FileList ) );
    if ( file_list == NULL ) {
        fprintf( stderr, "Could not allocate memory for file list\n" );
        return NULL;
    }
    memset( file_list, '\0', sizeof( FileList ) );
    file_list->size = 0;

    dir_entry = readdir( dir_ptr );
    while ( dir_entry != NULL ) {

        /* Skip the parent directory and the current directory */
        if ( ( strncmp( dir_entry->d_name, ".", strlen( dir_entry->d_name ) ) == 0 ) ||
             ( strncmp( dir_entry->d_name, "..", strlen( dir_entry->d_name ) ) == 0 ) ) {
            dir_entry = readdir( dir_ptr );
            continue;
        }

        /* The first-letter check will do a fast compare to see if the full string
         * needs to be evaluated (looking for the watcher prefix */
        if ( prefix != NULL && dir_entry->d_name[0] == prefix[0] ) {
            /* Skip any previous archive files */
            if ( strstr( dir_entry->d_name, prefix ) != NULL ) {
                dir_entry = readdir( dir_ptr );
                continue;
            }
        }

        /* Perform the same exclusion check on 'dot' files */
        if ( prefix != NULL && dir_entry->d_name[0] == '.' ) {
            /* Skip any previous archive files */
            if ( strstr( dir_entry->d_name+1, prefix ) != NULL ) {
                dir_entry = readdir( dir_ptr );
                continue;
            }
        }

        /* Create a node for the current file and add it to the list */
        FileNode *file = malloc( sizeof( FileNode ) );
        if ( file == NULL ) {
            fprintf( stderr, "Could not allocate memory for file storage\n" );
            return NULL;
        }
        memset( file, '\0', sizeof( FileNode ) );
        sprintf( file->name, "%s", dir_entry->d_name );
        file->next = NULL;

        /* Read the status of the file to get size information */
        stat( file->name, &statbuf );
        file->size = statbuf.st_size;

        /* Get the file's checksum */
        memset( command, '\0', sizeof( command ) );
        sprintf( command, "md5sum %s", file->name );
        raw_checksum = popen( command, "r" );
        if ( raw_checksum == NULL ) {
            /* Warn, but don't die. This will just cause the system to run
             * a new archive */
            printf( "Failed to gather new checksum\n" );
        }
        fread( checksum_string, sizeof( char ), sizeof( checksum_string ),
               raw_checksum );
        pclose( raw_checksum );

        checksum = strtok( checksum_string, " " );
        if ( checksum == NULL ) {
            /* Again, this is not fatal. Just force a re-count */
            printf( "Failed to parse new checksum\n" );
        }
        sprintf( file->checksum, "%s", checksum );


        /* If this is the first node, then assign it to the head pos */
        if ( file_list->head == NULL ) {
            file_list->head = file;
            file_list->cursor = file_list->head;
            file_list->size = 1;
        } else {
            /* Else add it to the end of the of list */
            file_list->cursor = file_list->head;
            while ( file_list->cursor->next != NULL ) {
                file_list->cursor = file_list->cursor->next;
            }
            file_list->cursor->next = file;
            file_list->size += 1;
        }

        /* Read the next entry from the directory */
        dir_entry = readdir( dir_ptr );
    }

    closedir( dir_ptr );
    return file_list;
}


/***
 * Function: void destroy_file_list(FileList* files)
 * Inputs: FileList* files - a file list pointer returned from collect_files()
 * Outputs: NONE
 * Returns: NONE
 * Description: This function frees the resources associated with the given
 * file list. This function should be called to prevent memory leaks.
 ***/
void destroy_file_list( FileList * files )
{

    if ( files->size == 0 ) {
        return;
    }

    __reset_file_list( files );
    FileNode *delete;
    while ( files->cursor != NULL ) {
        delete = files->cursor;
        files->cursor = files->cursor->next;
        free( delete );
    }
    free( files );
}


/***
 * Function: char* archvie_files(FileList* files, const char* prefix)
 * Inputs:
 *    FileList* files - list of files returned from collect_files()
 *    char* prefix - base name of the archive file
 * Outputs: NONE
 * Returns: Name of archived file - success
 * Description: This function builds a TAR archive from the files specified in
 * FILES. The PREFIX is used to determine the name, while the system time is
 * used to mark when the archive was created.
 ***/
char *archive_files( FileList * files, const char *prefix )
{
    char date[TINYBUF] = { '\0' };
    char string[BUFSIZE] = { '\0' };
    static char file_name[SMALLBUF] = { '\0' };

    __get_time_of_day( date );
    sprintf( file_name, "%s_%s.tar", prefix, date );

    __reset_file_list( files );
    while ( files->cursor != NULL ) {
        sprintf( string, "tar -rvf %s %s > /dev/null", file_name,
                 files->cursor->name );
        system( string );
        files->cursor = files->cursor->next;
    }

    /* Store the file checksums for future runs */
    save_checksums( files, CHECKSUM_FILE );

    /* The archive file exists and should be included in the archive */
    if( file_exists(CHECKSUM_FILE) ){
        sprintf( string, "tar -rvf %s %s > /dev/null", file_name,
                 CHECKSUM_FILE );
        system( string );
    }

    return file_name;
}

/***
 * Function: bool files_changed(FileList* files, char* checksums)
 * Inputs:
 *    FileList* files - list of files from collect_files()
 *    char* checksums - Name of file containing checksums
 * Outputs: NONE
 * Returns:
 *  True  - if at least 1 file has changed
 *  False - if no files have changed
 * Description: This function will return true if any file in the file list has
 * changed since the last time the checksums were gathered.
 ***/
bool files_changed( FileList * files, const char *checksums )
{

    FILE *checksum_file = NULL;
    checksum_file = fopen( checksums, "r" );
    /* If there is no checksum file, assume that the files have changed */
    if ( checksum_file == NULL ) {
        return true;
    }

    char string[SMALLBUF];
    char *checksum = NULL;
    char *name = NULL;
    FileNode *file = NULL;

    /* Create a file tree to hold the date from the config file */
    FileList *old_list = malloc( sizeof( FileList ) );
    if ( old_list == NULL ) {
        fprintf( stderr, "Could not allocate memory for baseline file tree\n" );
        return true;
    }
    memset( old_list, '\0', sizeof( FileList ) );


    /* Read the checksums from the file into a storage tree */
    while ( !feof( checksum_file ) ) {
        fgets( string, sizeof( string ), checksum_file );

        /* Chomp the new line */
        strtok( string, "\n" );

        /* Split the line into a checksum and a file name */
        /* Any failure to get a checksum or file name will cause a new archive
         * to be made */
        checksum = strtok( string, " " );
        if ( checksum == NULL ) {
            break;
        }
        name = strtok( NULL, " " );
        if ( name == NULL ) {
            break;
        }
        file = malloc( sizeof( FileNode ) );
        if ( file == NULL ) {
            fprintf( stderr, "Could not allocate file change tree\n" );
            fclose( checksum_file );
            destroy_file_list( old_list );
            return true;
        }
        memset( file, '\0', sizeof( FileNode ) );

        /* Fill the tree node */
        sprintf( file->name, "%s", name );
        sprintf( file->checksum, "%s", checksum );
        file->next = NULL;

        if ( old_list->head == NULL ) {
            old_list->head = file;
            old_list->cursor = old_list->head;
            old_list->size = 1;
        } else {
            old_list->cursor = old_list->head;
            while ( old_list->cursor->next != NULL ) {
                old_list->cursor = old_list->cursor->next;
            }
            old_list->cursor->next = file;
            old_list->size += 1;
        }
    }
    fclose( checksum_file );

    /* Compare the checksums */
    __reset_file_list( old_list );
    __reset_file_list( files );

    /* If the the lists are not the same size, trigger a recount because something
     * was either added or deleted */
    if ( old_list->size != files->size ) {
        destroy_file_list( old_list );
        return true;
    }

    while ( files->cursor != NULL ) {
        while ( old_list->cursor != NULL ) {

            if ( strcmp( files->cursor->name, old_list->cursor->name ) == 0 ) {
                if ( strncmp
                     ( files->cursor->checksum, old_list->cursor->checksum,
                       strlen( old_list->cursor->checksum ) ) != 0 ) {
                    /* As soon as one checksum does not match, trigger an
                     * archive */
                    destroy_file_list( old_list );
                    return true;
                }
            }
            old_list->cursor = old_list->cursor->next;
        }
        __reset_file_list( old_list );
        files->cursor = files->cursor->next;
    }

    destroy_file_list( old_list );
    return false;
}

/***
 * Function: void save_checksums(FileList* files, char* checksums)
 * Inputs:
 *  FileList* files - list of files returned from collect_files()
 *  char* checksums - name of the file that should contain the checksums
 * Outputs: NONE
 * Returns: NONE
 * Description: This function will store a checksum for each file in file list
 * and write it to checksum file.
 ***/
void save_checksums( FileList * files, const char *checksums )
{

    /* Invariant: file list cannot be NULL */
    if ( files->head == NULL ) {
        fprintf( stderr, "Attempting to use NULL pointer in save_checksums\n" );
        return;
    }

    FILE *cmd_output = NULL;
    FILE *checksum_file = NULL;
    char result[SMALLBUF] = { '\0' };
    char command[BUFSIZE] = { '\0' };

    checksum_file = fopen( checksums, "w" );

    /* Get the checksum of each file */
    __reset_file_list( files );
    while ( files->cursor != NULL ) {
        memset( command, '\0', sizeof( command ) );
        memset( result, '\0', sizeof( result ) );
        sprintf( command, "md5sum %s", files->cursor->name );
        cmd_output = popen( command, "r" );
        if ( cmd_output == NULL ) {
            printf( "pipe failed\n" );
            return;
        }
        fread( result, sizeof( char ), sizeof( result ), cmd_output );
        pclose( cmd_output );
        fwrite( result, sizeof( char ), strlen( result ), checksum_file );
        files->cursor = files->cursor->next;
    }
    fclose( checksum_file );
}


/***
 * Function: bool blacklist_clear()
 * Inputs: struct config_t config - configuration structure containing the black
 * list.
 * Outputs: NONE
 * Returns:
 *  True - no black list programs are running
 *  False - at least one program on the black list is currently running.
 * Description: This function will check to see if any programs listed on the
 * start-up black list are active.
 ***/
bool blacklist_clear( struct config_t config, bool interactive )
{
    DispBuffer error_screen;
    char status_line[LINE_LENGTH] = {'\0'};

    int index = 0;
    int is_running = false;
    char command[SMALLBUF];
    int running_count = 0;
    error_screen = disp_allocate_lines();
    disp_add_new_line(error_screen);

    for ( index = 0; index < MAX_PRIORITIES; index++ ) {
        if ( !config.priority[index].should_block ) {
            continue;
        }
        /* The process should block, so check to see if it's running */
        memset( command, '\0', SMALLBUF );
        sprintf( command, "pgrep %s", config.priority[index].name );

        /* pgrep will return 0 if the process is found, so invert the signal */
        is_running = !system( command );
        if ( is_running ) {

            snprintf(status_line, LINE_LENGTH, "%s is running. Backup not allowed",
                     config.priority[index].name);
            disp_add_line(error_screen, status_line, strlen(status_line) );
            running_count++;
        }
    }

    if ( running_count == 0 ) {
        disp_allocate_lines(error_screen);
        /* None of the blacklist programs are running */
        return true;
    } else {
        /* Only draw the screen if running interactively */
        if( interactive ){
            disp_draw(error_screen);
            sleep(5);
        }
        disp_deallocate_lines(error_screen);
        return false;
    }
}

/***
 * Function: __get_time_of_day(char* date_string)
 * Description: Given a date string, return the time broken back into a formatted
 *              string.
 ***/
static void __get_time_of_day( char *date_string )
{

    /* First call time() to get a raw time */
    time_t raw_time = time( NULL );

    /* Now post-process the raw time to get a broken-down time */
    struct tm *broken_time = malloc( sizeof( struct tm ) );
    memset( broken_time, '\0', sizeof( struct tm ) );
    localtime_r( &raw_time, broken_time );

    sprintf( date_string,
             "D%02d_M%02d_Y%02d_H%02d_m%02d_s%02d",
             broken_time->tm_mday, broken_time->tm_mon + 1,
             /* If you're maintaining this after this calculation is no longer true,
              * HOORAY FOR ME! BTW, I'm long dead! */
             broken_time->tm_year - 100,
             broken_time->tm_hour, broken_time->tm_min, broken_time->tm_sec );

    free( broken_time );
}

/***
 * Function: void __reset_file_list(FileList* files)
 * Inputs: FileList* files - file list to reset
 * Description: Helper function to reset a file list cursor to the head
 ***/
static void __reset_file_list( FileList * files )
{
    files->cursor = files->head;
}
