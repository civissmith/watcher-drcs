/*******************************************************************************
* @Title: modes.h
*
* @Author: Phil Smith
*
* @Date: Tue, 28-Feb-17 04:19AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Export the two modes of program flow
*
*
*******************************************************************************/
#ifndef __WATCHER_MODES__
#define __WATCHER_MODES__

void interactive_mode( char *config_name );
void automatic_mode( char *config_name, bool interactive );
#endif                          /* __WATCHER_MODES__ */
