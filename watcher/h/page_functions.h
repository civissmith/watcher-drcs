/*******************************************************************************
* @Title: page_functions.h
*
* @Author: Phil Smith
*
* @Date: Thu, 02-Feb-17 05:14AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Exports the function prototypes for each page. These are the names
*           of the callbacks registered for each page.
*
*
*******************************************************************************/

#ifndef __WATCHER_PAGE_FUNCTIONS__
#define __WATCHER_PAGE_FUNCTIONS__

/* Define the keys for the pages */
enum { P_HELP,
    P_SELECTION_MENU,
    P_MAIN_MENU,
    P_BACKUP_0,
    P_RESTORE_0,
    P_LIST_0,
    P_EXIT,
    P_EXIT_CONFIRMED,
} page_numbers;

/* Define keyboard encodings */
#define CMD_QUIT   100
#define CMD_UP     101 /* Up Arrow */
#define CMD_DOWN   102 /* Down Arrow */
#define CMD_LEFT   103 /* Left Arrow */
#define CMD_RIGHT  104 /* Right Arrow */
#define CMD_ENTER  200 /* Enter Key */
#define CMD_ESCAPE 201 /* Escape Key */

/***
 * struct page_args_t:
 * Structure to contain metadata about the application. It includes the command
 * captured by the key processor, the name of the configuration file, the next
 * page that a page wants to jump to (for jump tree processing) and information
 * about the active backing store.
 ***/
struct page_args_t {
    int command;
    int next_page;
    char *config_name;
    char *target_store;
    int target_port;
};

/***
 * struct page_t:
 * Structure to encapsulate information about page elements.
 ***/
struct page_t {
    int ID;
    void ( *display ) ( struct page_args_t * args );
};


/* Page callbacks */
void selection_page( struct page_args_t *args );
void main_page( struct page_args_t *args );
void backup_page( struct page_args_t *args );
void help_page( struct page_args_t *args );
void exit_page( struct page_args_t *args );
void list_page( struct page_args_t *args );
void restore_page( struct page_args_t *args );


#endif                          // __WATCHER_PAGE_FUNCTIONS__
