/*******************************************************************************
* @Title: archive.h
*
* @Author: Phil Smith
*
* @Date: Tue, 14-Mar-17 10:36AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This head exports the API to use the archive function.
*
*
*******************************************************************************/
#ifndef __WATCHER_ARCHIVE__
#define __WATCHER_ARCHIVE__
#include <stdbool.h>

bool archive_unpack( const char *archive, const char *temp_dir );
#endif                          /* __WATCHER_ARCHIVE__ */
