/*******************************************************************************
* @Title: client_functions.h
*
* @Author: Phil Smith
*
* @Date: Tue, 20-Dec-16 07:50AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This module exports the API for interacting with "client functions".
*           These functions are used primarily by the automated backup process.
*
*
*******************************************************************************/
#ifndef __WATCHERD_FUNCTIONS__
#define __WATCHERD_FUNCTIONS__
#include <stdbool.h>
#include <limits.h>
#include "config.h"

typedef struct file_list_t FileList;
typedef struct file_node_t FileNode;

/***
 * function: FileList* collect_files(char* path, const char* prefix)
 * Description: This function returns a list of files in the directory specified
 * by PATH. Any file that begins with PREFIX is ignored. The returned file list
 * should be destroyed to prevent memory leaks.
 ***/
FileList *collect_files( char *path, const char *prefix );

/***
 * function: void destroy_file_list(FileList* files)
 * Description: This function frees the resources associated with the given
 * file list. This function should be called to prevent memory leaks.
 ***/
void destroy_file_list( FileList * files );

/***
 * function: char* archvie_files(FileList* files, const char* prefix)
 * Description: This function builds a TAR archive from the files specified in
 * FILES. The PREFIX is used to determine the name, while the system time is
 * used to mark when the archive was created.
 ***/
char *archive_files( FileList * files, const char *prefix );

/***
 * function: bool blacklist_clear()
 * Description: This function will check to see if any programs listed on the
 * start-up black list are active.
 ***/
bool blacklist_clear( struct config_t config, bool interactive );

/***
 * function: void save_checksums(FileList* files, char* checksums)
 * Description: This function will store a checksum for each file in file list
 * and write it to checksum file.
 ***/
void save_checksums( FileList * files, const char *checksums );

/***
 * function: bool files_changed(FileList* files, char* checksums)
 * Description: This function will return true if any file in the file list has
 * changed since the last time the checksums were gathered.
 ***/
bool files_changed( FileList * files, const char *checksums );

#endif                          /* __WATCHERD_FUNCTIONS__ */
