/*******************************************************************************
* @Title: client.c
*
* @Author: Phil Smith
*
* @Date: Thu, 15-Dec-16 07:22AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Implement the client API for the watcherd service
*
*
*******************************************************************************/
#include "client.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <syslog.h>
#include "common.h"


/***
 * struct client
 * Encapsulate context information about a client object.
 ***/
struct client {
    char host[TINYBUF];
    unsigned short port;
    char connected;
    int socket;
};

/***
 * Function: Client wa_get_client()
 * Inputs: NONE
 * Outputs: NONE
 * Returns: Null-initialized Client context
 * Description: This function returns a client context that can be used with
 * other wa_* functions. This function must be called BEFORE using any other
 * functions. The client context must be destroyed properly to prevent memory
 * leaks.
 ***/
Client wa_get_client(  )
{

    Client context = malloc( sizeof( struct client ) );
    /* Immediately fail if memory is not available */
    if ( context == NULL ) {
        return context;
    }

    /* Initialize the structure before returning */
    memset( context, '\0', sizeof( struct client ) );
    return context;
}

/***
 * Function: void wa_destroy_client(Client ctxt)
 * Inputs: Client ctxt - A client context returned from wa_get_client()
 * Outputs: NONE
 * Returns: NONE
 * Description: This function will free the resources associated with the
 * given client context. This function must be called when the context is no
 * longer needed to prevent memory leaks.
 ***/
void wa_destroy_client( Client ctxt )
{

    /* Invariant: client context cannot be NULL */
    if ( ctxt == NULL ) {
        return;
    }

    /* If the client was connected, disconnect it and close it's file handles */
    if ( ctxt->connected ) {
        close( ctxt->socket );
        ctxt->connected = false;
    }

    /* Free the client context */
    free( ctxt );
}


/***
 * Function: int wa_set_host(Client ctxt, char* host)
 * Inputs:
 *   Client ctxt - A client context returned from wa_get_client()
 *   char* host - A string containing the host name of a server
 *                Can be dotted-decimal or text host name
 * Outputs: NONE
 * Returns:
 *   0 - Success
 *  <0 - Failure (Host name was not associated correctly)
 * Description: This function associates the given host name to the provided
 * client context.
 ***/
int wa_set_host( Client ctxt, char *host )
{

    /* Invariant: client context cannot be NULL */
    if ( ctxt == NULL ) {
        return -1;
    }
    /* Invariant: host name must fit in host buffer */
    if ( strlen( host ) > TINYBUF ) {
        return -1;
    }
    strncpy( ctxt->host, host, strlen( host ) );
    return 0;
}


/***
 * Function: wa_set_client_port(Client ctxt, unsigned short port)
 * Description: Assigns the given port to the client context.
 ***/
int wa_set_client_port( Client ctxt, unsigned short port )
{

    /* Invariant: client context cannot be NULL */
    if ( ctxt == NULL ) {
        return -1;
    }

    ctxt->port = htons( port );
    return 0;
}


/***
 * Function: int wa_connect(Client ctxt)
 * Inputs: Client ctxt - A client context returned from wa_get_client()
 * Outputs: NONE
 * Returns:
 *  0 - Success
 * <0 - Failure
 * Description: This function will open the connection between the local
 * machine (client) and the associated server (host)
 ***/
int wa_connect( Client ctxt )
{

    /* Invariant: Client context must exist */
    if ( ctxt == NULL ) {
        return -1;
    }
    /* Invariant: Client cannot be connected */
    if ( ctxt->connected ) {
        return -1;
    }

    struct sockaddr_in svrAddr;
    int retval = 0;
    int optval = 1;
    struct addrinfo *addrList = NULL;
    struct addrinfo *thisAddr = NULL;
    struct addrinfo addrHint;
    char addrBuff[INET_ADDRSTRLEN] = { '\0' };
    const char *convAddr = NULL;
    struct sockaddr_in *sockAddr = NULL;
    int binAddr = 0x0100007F;


    /* Look for an interface that is IPv4, with a TCP connection */
    addrHint.ai_flags = AI_CANONNAME;
    addrHint.ai_family = AF_INET;
    addrHint.ai_socktype = SOCK_STREAM;
    addrHint.ai_protocol = 0;
    addrHint.ai_addrlen = INET_ADDRSTRLEN;
    addrHint.ai_canonname = NULL;
    addrHint.ai_addr = NULL;
    addrHint.ai_next = NULL;

    /* Request a TCP socket fd */
    ctxt->socket = socket( AF_INET, SOCK_STREAM, 0 );
    if ( ctxt->socket < 0 ) {
        syslog( LOG_ERR, "Client failed to get a socket file descriptor\n" );
        return -1;
    }

    /* Allow the TCP socket to be re-used right away */
    retval = setsockopt( ctxt->socket, SOL_SOCKET, SO_REUSEADDR, &optval,
                         sizeof( optval ) );
    if ( retval < 0 ) {
        syslog( LOG_ERR, "Client failed to set socket to re-usable\n" );
        return -1;
    }

    /* Use getaddrinfo() to find the right IP from host name or dotted notation */
    retval = getaddrinfo( ctxt->host, NULL, &addrHint, &addrList );
    if ( retval < 0 ) {
        syslog( LOG_ERR, "Could not get address info for host [%s]\n",
                ctxt->host );
        return -1;
    }

    /* Iterate over the list to find the specific address info */
    for ( thisAddr = addrList; thisAddr != NULL; thisAddr = thisAddr->ai_next ) {

        /* Coerce thisAddr into a sockaddr_in so inet_ntop returns an IPv4 address */
        sockAddr = ( struct sockaddr_in * )thisAddr->ai_addr;

        convAddr =
            inet_ntop( AF_INET, &sockAddr->sin_addr, addrBuff,
                       INET_ADDRSTRLEN );
        if ( convAddr == NULL ) {
            /* If the hostname is invalid - stop */
            syslog( LOG_ERR,
                    "Could not convert \"%s\" to IP address. Aborting.\n",
                    ctxt->host );
            return -1;
        }
    }
    /* Free the list of potential address */
    freeaddrinfo( addrList );

    retval = inet_pton( AF_INET, convAddr, &binAddr );
    if ( retval != 1 ) {
        syslog( LOG_ERR, "Error converting \"%s\" to binary network address\n",
                convAddr );
        return -1;
    }

    /* Setup the server parameters */
    svrAddr.sin_family = AF_INET;
    svrAddr.sin_port = ctxt->port;
    svrAddr.sin_addr.s_addr = binAddr;
    memset( svrAddr.sin_zero, 0, sizeof( svrAddr.sin_zero ) );


    /* Connect to the server */
    retval =
        connect( ctxt->socket, ( struct sockaddr * )&svrAddr,
                 sizeof( svrAddr ) );
    if ( retval < 0 ) {
        syslog( LOG_ERR, "Client could not connect to server\n" );
        return -1;
    }

    /* Mark the connection as established so that re-connects will fail */
    ctxt->connected = true;

    return 0;
}


/***
 * Function: ssize_t wa_transmit(Client ctxt, char* buffer, size_t size)
 * Inputs:
 *  Client ctxt - A client context returned from wa_get_client()
 *  char* buffer - A buffer containing the information to send
 *  size_t size - The amount of data to send from the buffer
 * Outputs: NONE
 * Returns:
 *   >= 0 - Success (size of data actually sent to host)
 *   < 0 - Failure (some or all data may not have been sent)
 * Description: This function will send SIZE bytes from the buffer pointed to
 * by BUFFER to the host associated in the client context. NOTE: This function
 * does NO SIZE VALIDITY checks. The user must ensure that SIZE is within the
 * constraints of BUFFER.
 ***/
ssize_t wa_transmit( Client ctxt, char *buffer, size_t size )
{

    /* Invariant: Client must exist */
    if ( ctxt == NULL ) {
        return -1;
    }
    /* Invariant: Client must be connected */
    if ( ctxt->connected != true ) {
        return -1;
    }

    ssize_t actual = 0;
    ssize_t commanded = ( ssize_t ) size;

    actual = send( ctxt->socket, ( void * )buffer, commanded, 0 );
    while ( actual != commanded ) {
        commanded -= actual;
        actual =
            send( ctxt->socket, ( void * )( buffer + actual ), commanded, 0 );
        if ( actual < 0 ) {
            syslog( LOG_ERR, "Transmission failed\n" );
            return actual;
        }
    }
    return actual;
}

/***
 * Function: ssize_t wa_receive(ctxt, buffer, size)
 * Inputs: Client context, Byte buffer, signed size
 * Outputs: NONE
 * Returns: Number of bytes received - Success, -1 - Error
 * Description: Receives SIZE bytes into BUFFER from the connected host. Failure
 *              to call connect before this will result in an error.
 ***/
ssize_t wa_receive( Client ctxt, char *buffer, size_t size )
{

    /* Invariant: Client must exist */
    if ( ctxt == NULL ) {
        return -1;
    }
    /* Invariant: Client must be connected */
    if ( ctxt->connected != true ) {
        return -1;
    }

    return recv( ctxt->socket, buffer, size, 0 );
}
