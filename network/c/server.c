/*******************************************************************************
* @Title: server.c
*
* @Author: Phil Smith
*
* @Date: Thu, 15-Dec-16 12:17PM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Implement the server API for the watchers service
*
*
*******************************************************************************/
/* Include additional feature test macros for older GLIBC versions */
#if __GNUC__ < 5
#define _XOPEN_SOURCE 500
#include <strings.h>
#endif
#include "server.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <stdbool.h>
#include <pthread.h>
#include <syslog.h>
#include <dirent.h>
#include "protocol.h"
#include "common.h"
#include "config.h"

/***
 * struct server_t
 * Encapsulate context information about a server object.
 ***/
struct server_t {
    unsigned short port;
    void ( *server_function );
    void *server_args;
};

/***
 * Function: wa_get_server()
 * Description: Allocate the resources for a new server object and return a
 *              context referring to it.
 ***/
Server *wa_get_server(  )
{

    Server *server = malloc( sizeof( Server ) );
    if ( server == NULL ) {
        return NULL;
    }

    /* Initialize the structure to all NULL before returning */
    memset( server, '\0', sizeof( Server ) );
    return server;

}

/***
 * Function: wa_destroy_server(Server* ctxt)
 * Description: Free the resources associated with the given server context. This
 *              function must be called when finished with a server or else leakage
 *              will occur.
 ***/
void wa_destroy_server( Server * ctxt )
{
    /* Invariant: Context cannot be null */
    if ( ctxt == NULL ) {
        return;
    }
    free( ctxt );
}

/***
 * Function: wa_set_server_port(Server* ctxt, unsigned short port)
 * Description: Assigns the port argument to the given server.
 ***/
int wa_set_server_port( Server * ctxt, unsigned short port )
{
    /* Invariant: Server must exist */
    if ( ctxt == NULL ) {
        return -1;
    }

    ctxt->port = htons( port );
    return 0;
}

/***
 * Function: wa_register_server(Server* ctxt, void* function)
 * Description: Registers the given function as the callback function for the
 *              server. This function will be called once the server is ready
 *              to listen.
 ***/
int wa_register_server( Server * ctxt, void *function )
{
    /* Invariant: Server must exist */
    if ( ctxt == NULL ) {
        return -1;
    }

    ctxt->server_function = function;
    return 0;
}

/***
 * Function: wa_set_server_args(Server* ctxt, void* args)
 * Description: Adds the arguments to the server context. These arguments will
 *              be provided to the server's callback function at each call.
 ***/
int wa_set_server_args( Server * ctxt, void* args )
{
    /* Invariant: Server must exist */
    if ( ctxt == NULL ) {
        return -1;
    }

    ctxt->server_args = args;
    return 0;
}

/***
 * Function: wa_server(Server* ctxt)
 * Description: Begin the run-time loop for the server. This function will not
 *              return if successful.
 ***/
int wa_serve( Server * ctxt )
{

    /* Invariant: Server must exist */
    if ( ctxt == NULL ) {
        syslog( LOG_ERR, "wa_serve called with no server object" );
        return -1;
    }

    /* Test file to write */

    /* cliAddr: sockaddr structure to listen for clients */
    struct sockaddr_in cliAddr;
    /* cliSize: size of the accepted client address */
    socklen_t cliSize = 0;
    /* gpSocket: General purpose socket that listens for clients */
    int gpSocket = 0;
    /* cliSocket: Socket that accepts clients */
    int cliSocket = 0;
    /* retVal: General return value catcher */
    int retVal = 0;
    /* optVal: used to turn on SO_REUSEADDR */
    socklen_t optVal = 1;
    struct config_t* config = (struct config_t*) ctxt->server_args;



    /* Set the general purpose socket ready to listen: IPv4, TCP, default protocol */
    gpSocket = socket( AF_INET, SOCK_STREAM, 0 );
    if ( gpSocket < 0 ) {
        syslog( LOG_ERR, "Failed to create general purpose socket" );
        return ( -1 );
    }

    /* Set socket to be resuable */
    retVal =
        setsockopt( gpSocket, SOL_SOCKET, SO_REUSEADDR, &optVal,
                    sizeof( optVal ) );
    if ( retVal < 0 ) {
        syslog( LOG_ERR, "Failed to set socket options" );
        return ( -1 );
    }

    /* Set the parameters for the client connections */
    cliAddr.sin_family = AF_INET;
    cliAddr.sin_port = ctxt->port;
    cliAddr.sin_addr.s_addr = INADDR_ANY;
    memset( cliAddr.sin_zero, 0, sizeof( cliAddr.sin_zero ) );

    /* Now that the client parameters are set, bind the interface */
    retVal = bind( gpSocket, ( struct sockaddr * )&cliAddr, sizeof( cliAddr ) );
    if ( retVal < 0 ) {
        syslog( LOG_ERR, "Failed to bind to socket" );
        return ( -1 );
    }

    /* The server is bound, now listen for connections with no backlog */
    retVal = listen( gpSocket, 10 );
    if ( retVal < 0 ) {
        syslog( LOG_ERR, "Could not listen" );
        return ( -1 );
    }

    pthread_t handler_thread;
    pthread_attr_t handler_attr;
    pthread_attr_init( &handler_attr );
    pthread_attr_setdetachstate( &handler_attr, PTHREAD_CREATE_DETACHED );

    /* Run loop */
    while ( 1 ) {

        /* Accept a connection */
        cliSocket = accept( gpSocket, ( struct sockaddr * )&cliAddr, &cliSize );
        if ( cliSocket < 0 ) {
            break;
        }

        config->socket_fd = cliSocket;
        pthread_create( &handler_thread, &handler_attr, ctxt->server_function,
                        config );

    }

    /* Since gpSocket was opened, close it */
    close( gpSocket );

    /* Silence compiler (-Wreturn-type) */
    return ( 0 );

}
