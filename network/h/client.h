/*******************************************************************************
* @Title: client.h
*
* @Author: Phil Smith
*
* @Date: Thu, 15-Dec-16 07:22AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Expose the client API for the watcherd service
*
*
*******************************************************************************/
#ifndef __WATCHERD_CLIENT__
#define __WATCHERD_CLIENT__
#include <stdlib.h>

typedef struct client *Client;

/***
 * Function: Client wa_get_client()
 * Inputs: NONE
 * Outputs: NONE
 * Returns: Client context = Success, NULL = Error
 * Description: Return a client context to be used with client API.
 ***/
Client wa_get_client(  );

/***
 * Function: void wa_destroy_client()
 * Inputs: Client context
 * Outputs: NONE
 * Returns: NONE
 * Description: Free the resources associated with the given client. No more
 *              calls should be made on that client.
 ***/
void wa_destroy_client(  );

/***
 * Function: int wa_set_host(ctxt, host)
 * Inputs: Client context, String host name
 * Outputs: NONE
 * Returns: -1 - Error
 * Description: Given a client context, set the host name to HOST.
 ***/
int wa_set_host( Client ctxt, char *host );

/***
 * Function: int wa_set_client_port(ctxt, port)
 * Inputs: Client context, unsigned short port
 * Outputs: NONE
 * Returns: 0 - Success, -1 - Error
 * Description: Given a client context, set the transmit port
 ***/
int wa_set_client_port( Client ctxt, unsigned short port );


/***
 * Function: int wa_connect(ctxt)
 * Inputs:Client context
 * Outputs: None
 * Returns: 0 - Success, -1 - Error
 * Description: Connects the given client to its host.
 ***/
int wa_connect( Client ctxt );

/***
 * Function: ssize_t wa_transmit(ctxt, buffer, size)
 * Inputs: Client context, Byte buffer, signed size
 * Outputs: NONE
 * Returns: Number of bytes sent - Success, -1 - Error
 * Description: Transmits SIZE bytes from BUFFER to the connected host. Failure
 *              to call connect before this will result in an error.
 ***/
ssize_t wa_transmit( Client ctxt, char *buffer, size_t size );

/***
 * Function: ssize_t wa_receive(ctxt, buffer, size)
 * Inputs: Client context, Byte buffer, signed size
 * Outputs: NONE
 * Returns: Number of bytes received - Success, -1 - Error
 * Description: Receives SIZE bytes into BUFFER from the connected host. Failure
 *              to call connect before this will result in an error.
 ***/
ssize_t wa_receive( Client ctxt, char *buffer, size_t size );

#endif                          /* __WATCHERD_CLIENT__ */
