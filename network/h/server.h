/*******************************************************************************
* @Title: server.h
*
* @Author: Phil Smith
*
* @Date: Thu, 15-Dec-16 12:17PM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Expose the server API for the watchers service
*
*
*******************************************************************************/
#ifndef __WATCHERS_SERVER__
#define __WATCHERS_SERVER__

/***
 * struct Server
 * Opaque type to contain context information about a server object.
 ***/
typedef struct server_t Server;

/***
 * Function: Server wa_get_server()
 * Inputs: NONE
 * Outputs: NONE
 * Returns: Server context - success, NULL - failure
 * Description: Return a server context to the caller. The context can be used
 *              for subsequent API calls.
 ***/
Server *wa_get_server(  );

/***
 * Function: void wa_destroy_server(ctxt)
 * Inputs: Server context
 * Outputs: NONE
 * Returns: NONE
 * Description: Frees the resources for the given server context. No more API
 *              functions should be called on the object once destroyed.
 ***/
void wa_destroy_server( Server * ctxt );

/***
 * Function: wa_set_server_args(Server* ctxt, void* args)
 * Inputs: Server* ctxt, void* args
 * Outputs: NONE
 * Returns: 0 - success, <0 - failure
 * Description: Adds the args data to the server context. These arguments will
 *              be provided to the server callback function.
 ***/
int wa_set_server_args( Server * ctxt, void* args );

/***
 * Function: wa_register_server(Server* ctxt, void* function)
 * Inputs: Server* ctxt, void* function
 * Outputs: NONE
 * Returns: 0 - success, <0 - failure
 * Description: Registers the provided function as the callback for the server.
 *              This function will be called when the server object is ready
 *              to listen.
 ***/
int wa_register_server( Server * ctxt, void *function );

/***
 * Function: wa_set_server_port(Server* ctxt, unsigned short port)
 * Inputs: Server* ctxt, unsigned short port
 * Outputs: NONE
 * Returns: 0 - success, <0 - failure
 * Description: Assigns the port to which the server will attempt to bind. This
 *              library will not attempt to gain privilege if the value is a
 *              "well-defined" port. That must be handled by the application.
 ***/
int wa_set_server_port( Server * ctxt, unsigned short port );
/***
 * Function: wa_serve(Server* ctxt)
 * Inputs: Server* ctxt
 * Outputs: NONE
 * Returns: <0 - failure, Does not return if successful
 * Description: Begin the server's run-time loop. If no errors occur, this will
 *              start the cyclic listening behavior. This function will NOT return
 *              under normal circumstances.
 ***/
int wa_serve( Server * ctxt );
#endif                          /* __WATCHERS_SERVER__ */
