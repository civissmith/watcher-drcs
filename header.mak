################################################################################
# @Title: header.mak
#
# @Author: Phil Smith
#
# @Date: Mon, 27-Feb-17 07:15AM
#
# @Project: DC-K-00661 - Enhanced Backup Scripts
#
# @Purpose: Pre-compilation build rules
#
#
################################################################################
#
# Find the source code in the module
#

#
# makefiles strips header.mak and footer.mak (and duplicates thereof) out of the MAKEFILE_LIST
# and yields a string of Makefiles with full paths.
#
makefiles  = $(filter-out %/header.mak %/footer.mak,$(MAKEFILE_LIST))

#
# last_makefile finds the last makefile makefiles, strips the "Makefile" off the end
# of the path, then does a pattern substitution to strip the trailing '/'.
#
last_makefile = $(patsubst %/,%,$(dir $(word $(words $(makefiles)), $(makefiles))))

#
# subdirectory is used by the module-level Makefiles to get their own
# path to their source. The module-level files expect $(subdirectory) to be
# a relative path from the top-level with no trailing slash.
#
subdirectory = $(last_makefile)


#
# Set the object file output directory
#
# BINARY_DIR is set in (env.sh)
object_dir := $(BINARY_DIR)/$(subdirectory)

# LIBRARY_DIR is set in (env.sh)
library_dir := $(LIBRARY_DIR)

#
# Both of these targets are for an empty lock file. This file must be created
# so that the '.l' file can be used for dependency age calculations.
#
$(object_dir)/.l: ; @if !([ -e $@ ]); then ( $(MD) $(patsubst %/.l,%,$@) ; touch $@ ); fi
