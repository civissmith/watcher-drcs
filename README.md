# Watcher Distributed Revision Control System #
## Building The Software ##
The software build environment assumes you are using a BASH shell

1. Change into the directory containing the source code (typically *cd watcher-drcs*)
2. Source the **env.sh** environment configuration (*source env.sh*)
3. Run the build command (*build*)
4. The build is complete after the *Watcher build complete* and
   *Watcherd build complete* messages appear.

The executables *watcher* (client) and *watcherd* (server) will be deposited
in *watcher-drcs/bin/exe*.

## Running the Watcher Client ##
The client can either be installed onto a machine **make install_watcher** or
it can be run from a source directory. To run from a local source directory:

1. If not done already (like as part of a build), source the environment
   configuration (*source env.sh*)
    - This must be done so the executable path is updated to see the application
      and the library path is updated to see the shared libraries.
2. Run the client from the *watcher-drcs* directory. (*bin/exe/watcher -c /path/to/config*)
    - The client must have a configuration file
    - The default configuration is ~/.watcher-config
    - For testing purposes, it is safe to use or modify the config_files/watcher-config
      included in the *watcher-drcs* directory
3. The application will launch and can be controlled by following the prompts


## Running the Watcherd Server ##
The server can either be intalled onto a machie **make install_watcherd** or
it can be run from a source directory. To run from a local source directory:

1. If not done already (like as part of a build), source the environment
   configuration (*source env.sh*)
2. Run the server from the *watcher-drcs* directory. (*bin/exe/watcherd -n -c /path/to/config*)
    - The server must have a configuration file
    - For testing purposes, it is safe to use or modify the config_files/watcherd-config
    - The -n flag is used to prevent the server from trying to become a daemon
    - The server will not display any status information. Use CTRL+C to stop it
      when complete.

### What is this repository for? ###
The Watcher Distributed Revision Control System (DRCS) is minimal-featured revision
control system. It is capable of providing redundant backups of user data by
saving multiple copies of *archives* on several backup servers (or *backing stores*).

The Watcher DRCS is not meant to be a replacement for other technologies and
lacks many of the features one would expect from systems such as Subversion or
Git. The intent of the Watcher is to provide a mechanism to backup data that
requires nothing more than standard functions provided by Glibc. To that end,
many libraries (such as Ncurses) are not used as a matter of design.