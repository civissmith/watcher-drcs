#!/bin/bash
################################################################################
# @Title: env.sh
#
# @Author: Phil Smith
#
# @Date: Mon, 27-Feb-17 09:01AM
#
# @Project: DC-K-00661 - Enhanced Backup Scripts
#
# @Purpose: Setup the build environment for the Watcher DCRS
#
#
################################################################################

export BUILD_BASE=`pwd`
export BINARY_DIR="$BUILD_BASE/bin"
export LIBRARY_DIR="$BINARY_DIR/lib"
export EXE_DIR="$BINARY_DIR/exe"
export LD_LIBRARY_PATH="$LIBRARY_DIR"

# Add the build scripts to the environment
export PATH=$PATH":$BUILD_BASE/scripts"
export PS1="[watcher] $PS1"

function build () {
    local owd=`pwd`
    cd $BUILD_BASE
    make -j32
    cd $owd
}

function clean () {
    local owd=`pwd`
    cd $BUILD_BASE
    make clean
    cd $owd
}

# Add target environment variables
export TGT_CRON_FILE='/var/spool/cron/root'

# Add a GDB alias
function watcher_dbg () {
    sudo gdb -p `ps -ae | grep watcher | awk '{print $1}'`
}
