################################################################################
# @Title: Makefile
#
# @Author: Phil Smith
#
# @Date: Mon, 27-Feb-17 07:15AM
#
# @Project: DC-K-00661 - Enhanced Backup Scripts
#
# @Purpose: This file defines the top-level build architecture for the Watcher
#           Distributed Revision Control System
#
#
################################################################################
INSTALL_BIN := /usr/bin
INSTALL_LIB := /usr/lib
targets := $(EXE_DIR)/watcher
targets += $(EXE_DIR)/watcherd

#
# The all target is defined to be overridden by the module-level makefiles.
# Do not define any targets here.
#
.PHONY: all
all::

#
# Setup specific targets for each executable
#
watcher: $(EXE_DIR)/watcher
watcherd: $(EXE_DIR)/watcherd

#
# C compiler
#
CC := gcc

# C Flags
C_flags += -c
C_flags += -ggdb
C_flags += -fPIC
C_flags += -rdynamic
C_flags += -Wall


#
# Archiver
#
AR := gcc
AR_flags := -shared
AR_flags += -ggdb
AR_flags += -fPIC
AR_flags += -rdynamic
AR_flags += -o

#
# Define explicit rules for these command aliases
#
MD  := mkdir -p
MV  := mv -f
RM  := rm -rf
TST := test
SED := sed


#
# Create the library directory and executable directory
#
create-lib-dir := $(shell $(TST) -d $(LIBRARY_DIR) || $(MD) $(LIBRARY_DIR))
create-exe-dir := $(shell $(TST) -d $(EXE_DIR) || $(MD) $(EXE_DIR))


#
# Build the list of include files
#
include_dirs := $(BUILD_BASE)/network/h
include_dirs += $(BUILD_BASE)/display/h
include_dirs += $(BUILD_BASE)/common/h
include_dirs += $(BUILD_BASE)/watcher/h
include_dirs += $(BUILD_BASE)/watcherd/h

# Make all include dirs availabe too the compilers
include_flags += $(addprefix -I ,$(include_dirs))
vpath %.h $(include_dirs)


################################################################################
# Standard libraries needed for the DRCS
################################################################################
standard_libs += -pthread

################################################################################
# Libraries needed for the DRCS (this variable is filled in the footer)
# DO NOT delete this reference or else library list will be corrupted!
################################################################################
watcher_libs :=
libraries :=

################################################################################
# Object files needed for the DRCS (this variable is filled in the footer)
# DO NOT delete this reference or else object list will be corrupted!
################################################################################
objects :=

#
# The clean target is written so that it can be overridden by the module-level
# Makefiles.
#
.PHONY: clean
clean:: ; $(RM) $(BINARY_DIR)


################################################################################
# Define the source modules that need to be built.
################################################################################
#
# C Modules
#
source_dirs := watcher/c
source_dirs += watcherd/c
source_dirs += common/c
source_dirs += network/c
source_dirs += display/c


#
# Include the module-level makefiles into this file.
#
include $(addsuffix /Makefile,$(source_dirs))


#
# Include the Watcher and Watcherd executables into the target list
#
all:: $(targets)

#
# NOTE: footer.mak filters watcher.o, watcherd.o out of their respective
#       libraries so that there is no 'main' defined in ANY of the libraries.
#       Each object is a compilation target.
#

################################################################################
# Flags common to all of the executables
################################################################################
exe_flags := -ggdb
exe_flags += -rdynamic
exe_flags += $(includes)
exe_flags += $(standard_libs)
exe_flags += -L$(LIBRARY_DIR)
exe_flags += $(libraries)

#
# watcher: The watcher client executable
#
$(EXE_DIR)/watcher: watcher_flags += -o $(EXE_DIR)/watcher
$(EXE_DIR)/watcher: watcher_flags += $(BINARY_DIR)/watcher/c/watcher.o
$(EXE_DIR)/watcher: $(watcher_libs)
	@echo "=> Building the Watcher DRCS executive..."
	@$(CC) $(watcher_flags) $(exe_flags)
	@echo "=> Watcher build complete!"


#
# watcherd: The watcher daemon server
#
$(EXE_DIR)/watcherd: watcherd_flags += -o $(EXE_DIR)/watcherd
$(EXE_DIR)/watcherd: watcherd_flags += $(BINARY_DIR)/watcherd/c/watcherd.o
$(EXE_DIR)/watcherd: $(watcher_libs)
	@echo "=> Building the Watcher DRCS daemon..."
	@$(CC) $(watcherd_flags) $(exe_flags)
	@echo "=> Watcherd build complete!"

#
# install_watcher: Install the Watcher client component
#
.PHONY: install_watcher
install_watcher:
	@echo "Installing Watcher DRCS Client"
	@cp $(BUILD_BASE)/bin/exe/watcher $(INSTALL_BIN)
	@cp $(BUILD_BASE)/config_files/watcher-config $(HOME)/.watcher-config
	@echo "Installing Watcher DRCS Client libraries"
	@cp $(BUILD_BASE)/bin/lib/*.so $(INSTALL_LIB)
	@chmod 775 $(INSTALL_BIN)/watcher
	@echo "Installing Watcher server tester"
	@cp $(BUILD_BASE)/scripts/TestClient.py $(INSTALL_LIB)/python2.4/site-packages
	@cp $(BUILD_BASE)/scripts/server_test.py $(INSTALL_BIN)/watcher_tester
	@chmod 775 $(INSTALL_BIN)/watcher_tester
	@echo "Configuring watcher to backup periodically"
	@printf "# Periodically (10 minutes) command Watcher backup [watcher_client]\n" >> $(TGT_CRON_FILE)
	@printf "*/10 * * * * $(INSTALL_BIN)/watcher -a #[watcher_client]\n" >> $(TGT_CRON_FILE)

#
# remove_watcher: Remove the Watcher client component
#
.PHONY: remove_watcher
remove_watcher:
	@echo "Removing Watcher DRCS Client Executable"
	@rm $(INSTALL_BIN)/watcher
	@sed -i '/watcher_client/d' $(TGT_CRON_FILE)
	@echo "Removing Watcher server tester"
	@rm $(INSTALL_LIB)/python2.4/site-packages/TestClient.py
	@rm $(INSTALL_BIN)/watcher_tester

#
# install_watcherd: Install the Watcherd server component
#

.PHONY: install_watcherd
install_watcherd:
	@echo "Installing Watcher DRCS Server"
	@cp $(BUILD_BASE)/config_files/watcherd-config /etc/sysconfig/watcherd-config
	@chmod 755 /etc/sysconfig/watcherd-config
	@cp $(BUILD_BASE)/bin/exe/watcherd $(INSTALL_BIN)/watcherd
	@chmod 755 $(INSTALL_BIN)/watcherd
	@echo "Installing Watcher DRCS Server libraries"
	@cp $(BUILD_BASE)/bin/lib/*.so $(INSTALL_LIB)
	@echo "Configuring service to restart automatically"
	@printf "# Restart the Watcher daemon automatically [watcherd_server]\n" >> $(TGT_CRON_FILE)
	@printf "* * * * * $(INSTALL_BIN)/watcherd   #[watcherd_server]\n" >> $(TGT_CRON_FILE)


#
# remove_watcherd: Remove the Watcherd server component
#
.PHONY: remove_watcherd
remove_watcherd:
	@echo "Removing watcherd executable"
	@$(RM) $(INSTALL_BIN)/watcherd
	@sed -i '/watcherd_server/d' $(TGT_CRON_FILE)

#
# purge_watcherd: Remove the Watcherd server component and configuration data
#
.PHONY: purge_watcherd
purge_watcherd:
	@echo "Removing watcherd executable"
	@$(RM) $(INSTALL_BIN)/watcherd
	@sed -i '/watcherd_server/d' $(TGT_CRON_FILE)
	@echo "Removing watcherd configuration data"
	@$(RM) /etc/sysconfig/watcherd-config
