/*******************************************************************************
* @Title: file.c
*
* @Author: Phil Smith
*
* @Date: Fri, 17-Mar-17 10:09AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This module contains functions used interact with files in the
*           file system.
*
*******************************************************************************/
#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <string.h>
#include <ftw.h>
#include "file.h"


/***
 * Function: file_remove( const char *fpath, const struct stat *sb, int typeflag,
 *                        struct FTW *ftwbuf )
 * Description: This is the callback to nftw. When called, it will remove every
 *              file passed in.
 ***/
int file_remove( const char *fpath, const struct stat *sb, int typeflag,
                 struct FTW *ftwbuf )
{
    if ( typeflag == FTW_F ) {
        unlink( fpath );
    }
    return 0;
}

/***
 * Function: file_remove_dir(char* target)
 * Description: Given a directory, recursively remove every file it contains and
 *              then the directory itself.
 ***/
void file_remove_dir( const char *target )
{

    /* Invariant: target cannot be NULL */
    if ( target == NULL ) {
        return;
    }

    nftw( target, file_remove, 256,
          FTW_CHDIR | FTW_DEPTH | FTW_MOUNT | FTW_PHYS );
    rmdir( target );
}


/***
 * Function: file_exists(const char* name)
 * Description: Returns true if the given file exists or false otherwise.
 ***/
bool file_exists(const char* name){

    FILE* file = fopen(name, "r");

    if( file == NULL ){
        return false;
    }

    fclose(file);
    return true;
}
