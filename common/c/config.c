/*******************************************************************************
* @Title: config.c
*
* @Author: Phil Smith
*
* @Date: Wed, 01-Mar-17 03:08AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Define the methods/variables used to manage configuration files.
*
*
*******************************************************************************/
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdbool.h>
#include <dirent.h>
#include <time.h>
#include "config.h"
#include "common.h"
#include <errno.h>


static int __split_address( char *addr_string, struct address_t *addr );


/***
 * function: int parse_config(char* path, struct config_t* config)
 * Inputs:
 *  char* path - path to the configuration file
 * Outputs:
 *  struct config_t* config - structure containing an in-code version of the
 *  configuration file
 * Returns: 0 - Success, <0 - error
 * Description: This function will read the configuration from the specified
 * PATH. The contents of the file are stored in the provided config structure.
 ***/
int parse_config( char *path, struct config_t *config )
{

    char input[BUFSIZE];
    char *token;
    char *token2;
    char *value;
    char *save_ptr;
    char *alternate_backups;
    int index = 0;
    struct dir_entry_t *this_dir = NULL;
    char temp_string[PATH_MAX] = { '\0' };
    const unsigned short DEFAULT_PORT = 50050;
    int retval = 0;

    FILE *config_file = fopen( path, "r" );
    if ( config_file == NULL ) {
        fprintf(stderr, "Could not open file: %s\n", path);
        return -1;
    }

    config->alternate_count = 0;
    /* By default, the quota is disabled */
    config->quota = QUOTA_DISABLED;

    /* Prep the target dirs list to start adding diretories */
    config->target_dirs.head = NULL;
    config->target_dirs.cursor = NULL;

    while ( !feof( config_file ) ) {
        memset( input, '\0', BUFSIZE );
        fgets( input, BUFSIZE, config_file );

        /* Allow line comments */
        if ( input[0] == '#' || input[0] == '\n' ) {
            continue;
        }

        /* Parse out the config parameters */
        token = strtok_r( input, " =", &save_ptr );
        if ( token == NULL ) {
            continue;
        }


        if ( strncasecmp( token, "TARGET_DIR", strlen( "TARGET_DIR" ) ) == 0 ) {

            /* Create a directory entry to hold the parsed directory name */
            this_dir = malloc( sizeof( struct dir_entry_t ) );
            if ( this_dir == NULL ) {
                fprintf( stderr,
                         "Could not allocate memory for a target directory" );
                return -1;
            }
            memset( this_dir, '\0', sizeof( struct dir_entry_t ) );
            this_dir->next = NULL;

            /* Clear the temporary string */
            memset( temp_string, '\0', sizeof( temp_string ) );

            /* Parse the directory path out of the config file */
            value = strtok_r( NULL, " =", &save_ptr );
            snprintf( this_dir->path_name, NAME_MAX, "%s", value );

            /* The last character is probably a new line. It must be chomped. */
            strtok( this_dir->path_name, "\n" );

            /* Load the temp string with the path for tokenizing */
            memcpy( temp_string, this_dir->path_name, sizeof( temp_string ) );

            /* Tokenize the temp string to find the last name component */
            token2 = strtok( temp_string, "/" );
            while ( token2 != NULL ) {
                snprintf( this_dir->dir_name, sizeof( this_dir->dir_name ),
                          "%s", token2 );
                token2 = strtok( NULL, "/" );
            }

            /* Add the path name to the list */
            if ( config->target_dirs.head == NULL ) {
                /* This is the first entry in the list */
                config->target_dirs.head = this_dir;
            } else {
                /* This entry must be appended to the list */
                config->target_dirs.cursor = config->target_dirs.head;
                while ( config->target_dirs.cursor->next != NULL ) {
                    config->target_dirs.cursor =
                        config->target_dirs.cursor->next;
                }
                config->target_dirs.cursor->next = this_dir;
                config->target_dirs.cursor = config->target_dirs.head;
            }
        }

        if ( strncasecmp( token, "PRIMARY_BACKUP", strlen( "PRIMARY_BACKUP" ) )
             == 0 ) {
            value = strtok_r( NULL, " =", &save_ptr );
            if ( value == NULL ) {
                fprintf( stderr, "Invalid primary backup specified\n" );
                exit( EXIT_FAILURE );
            }
            retval = __split_address( value, &config->primary_backup );
            if ( retval < 0 ) {
                fclose( config_file );
                return retval;
            }
        }
        if ( strncasecmp
             ( token, "ALTERNATE_BACKUPS",
               strlen( "ALTERNATE_BACKUPS" ) ) == 0 ) {
            index = 0;
            alternate_backups = strtok_r( NULL, " =", &save_ptr );

            char *alt_token;
            char *alt_save;
            /* There may be up to a limit of alternate backups listed on a single
             * line. */
            alt_token = strtok_r( alternate_backups, ", ", &alt_save );
            while ( alt_token != NULL && index < MAX_ALTERNATES ) {
                value = alt_token;

                /* Chomp the new line off of the name */
                strtok( value, "\n" );

                retval =
                    __split_address( value, &config->alternate_backups[index] );
                if ( retval < 0 ) {
                    fclose( config_file );
                    return retval;
                }
                config->alternate_count++;
                alt_token = strtok_r( NULL, ", ", &alt_save );
                index++;
            }
        }
        if ( strncasecmp( token, "PRIORITY", strlen( "PRIORITY" ) ) == 0 ) {
            value = strtok_r( NULL, " ,", &save_ptr );
            index = 0;
            while ( value != NULL && index < MAX_PRIORITIES ) {
                value = strtok_r( NULL, ", ", &save_ptr );
                if ( value != NULL ) {

                    /* Chomp the new line off of the name */
                    strtok( value, "\n" );
                    sprintf( config->priority[index].name, "%s", value );
                    config->priority[index].should_block = true;
                    index++;
                }
            }
        }

        /* Server Config */
        /* Look for the Quota */
        if ( strncasecmp( token, "QUOTA", strlen( "QUOTA" ) ) == 0 ) {
            value = strtok_r( NULL, " =", &save_ptr );

            /* Chomp the new line off of the value */
            strtok_r( value, "\n", &save_ptr );
            config->quota = atoi( value );
            if ( config->quota < MIN_SAFE_QUOTA &&
                 config->quota != QUOTA_DISABLED ) {
                fprintf( stderr,
                         "Warning: Minimum safety override triggered\n" );
                config->quota = MIN_SAFE_QUOTA;
            }
        }

        /* Look for the IP address location */
        if ( strncasecmp( token, "PORT", strlen( "PORT" ) ) == 0 ) {
            value = strtok_r( NULL, " =", &save_ptr );
            /* Chomp the new line off the value */
            strtok_r( value, "\n", &save_ptr );

            config->port = ( unsigned short )atoi( value );
            /* Make sure the given port is in the unprotected range */
            if ( config->port <= 0x0400 ) {
                config->port = DEFAULT_PORT;
            }
        }

        /* Look for temporary directory */
        if ( strncasecmp( token, "TEMP_DIR", strlen( "TEMP_DIR" ) ) ==
             0 ) {
            value = strtok_r( NULL, " =", &save_ptr );

            /* Chomp the new line off of the value */
            strtok_r( value, "\n", &save_ptr );
            strncpy( config->temp_dir, value,
                     sizeof( config->temp_dir ) );
        }

        /* Look for the Backing store location */
        if ( strncasecmp( token, "BACKING_STORE", strlen( "BACKING_STORE" ) ) ==
             0 ) {
            value = strtok_r( NULL, " =", &save_ptr );

            /* Chomp the new line off of the value */
            strtok_r( value, "\n", &save_ptr );
            strncpy( config->backing_store, value,
                     sizeof( config->backing_store ) );
        }

        /* Look for the Backing store location */
        if ( strncasecmp( token, "TEST_BACKING_STORE", strlen( "TEST_BACKING_STORE" ) ) ==
             0 ) {
            value = strtok_r( NULL, " =", &save_ptr );

            /* Chomp the new line off of the value */
            strtok_r( value, "\n", &save_ptr );
            strncpy( config->test_backing_store, value,
                     sizeof( config->test_backing_store ) );
        }
    }

    fclose( config_file );
    return 0;
}

void destroy_config( struct config_t *config )
{
    /* Invariant: Config cannot be null */
    if ( config == NULL ) {
        return;
    }

    struct dir_entry_t *delete;
    config->target_dirs.cursor = config->target_dirs.head;
    while ( config->target_dirs.cursor != NULL ) {
        delete = config->target_dirs.cursor;
        config->target_dirs.cursor = config->target_dirs.cursor->next;
        free( delete );
    }
    free( config );
}

/***
 * function: void __split_address(char* addr_string, struct address_t* addr)
 * Inputs: char* addr_string - address string to split in IP:PORT form
 * Outputs: struct address_t* addr - address structure to hold the IP and PORT
 * Returns: NONE
 * Description: This function will split the address string in IP:PORT form to
 * an IP and a PORT stored in provided address buffer.
 ***/
static int __split_address( char *addr_string, struct address_t *addr )
{

    char *save_ptr;
    char *address = NULL;
    char *port = NULL;

    address = strtok_r( addr_string, ":", &save_ptr );
    port = strtok_r( NULL, ":", &save_ptr );

    /* Only option is a valid return. Everything else is fatal */
    if ( address != NULL && port != NULL ) {
        sprintf( addr->host, "%s", address );

        /* Make sure the given port is in the unprotected range */
        if ( atoi( port ) > 0x0400 && atoi( port ) < 0xFFFF ) {
            addr->port = ( unsigned short )atoi( port );
        } else {
            fprintf( stderr, "Invalid port - %d\n", atoi( port ) );
            fprintf( stderr, "Port numbers must be on range (%d, %d]\n", 0x0400,
                     0xFFFF );
            return -1;
        }
        return 0;
    }

    fprintf( stderr, "Invalid host:port combination - %s\n", addr_string );
    return -1;

}
