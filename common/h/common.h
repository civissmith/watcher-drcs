/*******************************************************************************
* @Title: common.h
*
* @Author: Phil Smith
*
* @Date: Thu, 02-Mar-17 07:31AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: This header contains definitions that are common throughout the project.
*
*******************************************************************************/
#ifndef __WATCHER_COMMON__
#define __WATCHER_COMMON__

#define BUFSIZE   4096
#define SMALLBUF  1024
#define TINYBUF   512
#define LINE_LENGTH 256
#define NAME_LEN 128

#define DEFAULT_CONFIG ".watcher-config"
#define CHECKSUM_FILE  ".watcher_archive_checksums"
#define WATCHER_PREFIX "watcher_archive"
#define STOP_FILE      "/tmp/.stop_watcherd"
#endif                          /* __WATCHER_COMMON__ */
