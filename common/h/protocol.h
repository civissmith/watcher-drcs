/*******************************************************************************
* @Title: protocol.h
*
* @Author: Phil Smith
*
* @Date: Wed, 01-Mar-17 05:06AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Export symbols needed to adhere to the Watcher DRCS Protocol
*
*
*******************************************************************************/
#ifndef __WATCHER_PROTOCOL__
#define __WATCHER_PROTOCOL__

#define W_PROTOCOL "WHEEL_V2"
#define W_SEND "WHEEL_V2 SEND"
#define W_LIST "WHEEL_V2 LIST"
#define W_RESTORE "WHEEL_V2 RESTORE"
#define W_TEST_START "WHEEL_V2 TEST_START"
#define W_TEST_STOP "WHEEL_V2 TEST_STOP"
#endif                          /* __WATCHER_PROTOCOL__ */
