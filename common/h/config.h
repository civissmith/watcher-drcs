/*******************************************************************************
* @Title: config.h
*
* @Author: Phil Smith
*
* @Date: Wed, 01-Mar-17 03:08AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Export the methods/variables used to manage configuration files.
*
*
*******************************************************************************/
#ifndef __WATCHER_CONFIG__
#define __WATCHER_CONFIG__
#include <stdbool.h>
#include <limits.h>
#include "common.h"

#define MAX_ALTERNATES 10
#define MAX_PRIORITIES 10
#define QUOTA_DISABLED -1
#define MIN_SAFE_QUOTA 5
/* Encapsulate IP addresses (converted from HOST:PORT notation) */
struct address_t {
    char host[TINYBUF];
    unsigned short port;
};

/* Encapsulate a blacklist process */
struct process_t {
    char name[TINYBUF];
    bool should_block;
};

/* Encapsulate the directories in the target dirs list */
struct dir_entry_t {
    char path_name[PATH_MAX];
    char dir_name[NAME_MAX];
    struct dir_entry_t *next;
};

/* Abstract the list of directores */
struct dir_list_t {
    struct dir_entry_t *head;
    struct dir_entry_t *cursor;
};


/* Encapsulate the information contained in the configuration file */
struct config_t {

    /* Server Config */
    int quota;
    unsigned short port;
    char backing_store[BUFSIZE];
    char test_backing_store[BUFSIZE];
    int socket_fd;

    /* Client Config */
    struct dir_list_t target_dirs;
    struct address_t primary_backup;
    struct address_t alternate_backups[MAX_ALTERNATES];
    struct process_t priority[MAX_PRIORITIES];
    char temp_dir[BUFSIZE];
    int alternate_count;
};

/***
 * Function: void parse_client_config(char* path, struct config_t* config)
 * Description: This function will read the configuration from the specified
 * PATH. The contents of the file are stored in the provided config structure.
 ***/
int parse_config( char *path, struct config_t *config );
/***
 * Function: destroy_config(struct config_t* config)
 * Description: Frees the resources used by the given resource.
 ***/
void destroy_config( struct config_t *config );
#endif                          /* __WATCHER_CONFIG__ */
