/*******************************************************************************
* @Title: file.h
*
* @Author: Phil Smith
*
* @Date: Fri, 17-Mar-17 10:09AM
*
* @Project: DC-K-00661 - Enhanced Backup Scripts
*
* @Purpose: Encapsulate functions dealing with file management.
*
*******************************************************************************/
#ifndef __WATCHER_FILE__
#define __WATCHER_FILE__
#include <stdbool.h>
void file_remove_dir( const char *target );
bool file_exists( const char* name );
#endif /* __WATCHER_FILE__ */
